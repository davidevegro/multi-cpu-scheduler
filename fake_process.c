#include <stdio.h>
#include <stdlib.h>
#include "fake_process.h"

#define LINE_LENGTH 1024

int FakeProcess_load(FakeProcess* p, const char* filename) {  
  FILE* f=fopen(filename, "r"); 
  if (! f)
    return -1;
  // read the PID
  char *buffer=NULL;
  size_t line_length=0;
  p->pid=-1;
  p->arrival_time=-1;
  List_init(&p->events);
  p->list.prev=p->list.next=0;
  int num_events=0;
  //setting fake_process and event stuff
  while (getline(&buffer, &line_length, f) >0){ 
    // got line in buf
    int pid=-1;
    int arrival_time=-1;
    int num_tokens=0;
    int duration=-1;
	

    num_tokens=sscanf(buffer, "PROCESS %d %d ", &pid, &arrival_time); 

    if (num_tokens==2 && p->pid<0){
      p->pid=pid;
      p->arrival_time=arrival_time;
      goto next_round;
    }
    num_tokens=sscanf(buffer, "CPU_BURST %d", &duration);
    if (num_tokens==1){
    
      ProcessEvent* e=(ProcessEvent*) malloc(sizeof(ProcessEvent));
      e->list.prev=e->list.next=0;
      e->type=CPU;
      e->duration=duration;
      List_pushBack(&p->events, (ListItem*)e);
      ++num_events;
      goto next_round;
    }
    num_tokens=sscanf(buffer, "DISK1 %d", &duration); 
    if (num_tokens==1){
   
      ProcessEvent* e=(ProcessEvent*) malloc(sizeof(ProcessEvent));
      e->list.prev=e->list.next=0;
      e->type=DISK1;
      e->duration=duration;
      List_pushBack(&p->events, (ListItem*)e);
      ++num_events;
      goto next_round;
    }
	num_tokens=sscanf(buffer, "DISK2 %d", &duration);
    if (num_tokens==1){
   
      ProcessEvent* e=(ProcessEvent*) malloc(sizeof(ProcessEvent));
      e->list.prev=e->list.next=0;
      e->type=DISK2;
      e->duration=duration;
      List_pushBack(&p->events, (ListItem*)e);
      ++num_events;
      goto next_round;
    }
	num_tokens=sscanf(buffer, "CAMERA %d", &duration); 
    if (num_tokens==1){
   
      ProcessEvent* e=(ProcessEvent*) malloc(sizeof(ProcessEvent));
      e->list.prev=e->list.next=0;
      e->type=CAMERA;
      e->duration=duration;
      List_pushBack(&p->events, (ListItem*)e);
      ++num_events;
      goto next_round;
    }
  next_round:

	; 
    
  }

	
  if (buffer)
    free(buffer);
  fclose(f);
  return num_events;
}






