#include "fake_process.h"
#include "linked_list.h"
#pragma once


typedef struct {
  ListItem list;
  int pid;
  ListHead events;
  int queue; //see fake_process.h
  int aging_aux; //variable used for aging policy in multilevel feedback queue scheduler
  int to_downgrade ; //set to 1 if cpu burst is higher than quantum(1/2/3) in mfq, else 0
  int last_task_IO;//set to 1 if last task was an IO burst
  int just_moved; //flag needed to update turnaround time when process is in queue
  int turnaround_time; //time to complete this process
  int waiting_time; //time in ready queue 
  int response_time;//time it tooks this process to get in a cpu after an I/O ended
} FakePCB;


struct FakeOS;
typedef void (*SchedulePreemptive)(struct FakeOS* os, void* args);
typedef void (*Scheduler)(struct FakeOS* os); //pointer to a scheduler function that doesnt need quantum args;

typedef struct FakeOS{
  FakePCB* running_one; //process running on cpu 1
  FakePCB* running_two;//process running on cpu 2
  FakePCB* running_three;//process running on cpu 3
  FakePCB* running_four;//process running on cpu 4
  ListHead queue[3];//queue[1] & queue[2] used for mfq scheduler
  int pids[100] ; //max 100 processes concurently running
  int pid_counter;
  ListHead disk_one_queue;//device queue DISK1
  ListHead disk_two_queue;//device queueDISK2
  ListHead camera_queue;//device queue CAMERA
  int timer; //OS time
  char scheduler[7];//type of scheduler choosen
  Scheduler schedule_fcfs;//pointer to fifo scheduler
  Scheduler schedule_sjf; //pointer to sjf scheduler
  SchedulePreemptive schedule_rr;//pointer to rr scheduler
  SchedulePreemptive schedule_mfq;//pointer to mfq scheduler
  void* schedulerr_args;// RR scheduler args
  void* schedulemfq_args;//MFQ scheduler args
  ListHead processes;

} FakeOS;

void FakeOS_init(FakeOS* os);
void FakeOS_simStep(FakeOS* os,FILE* fp);

