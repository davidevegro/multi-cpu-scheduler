#include <fcntl.h>
#include <mqueue.h> 
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#define FIFO "fifo"
#define SJF "sjf"
#define RR "rr"
#define MFQ "mfq"


#define QUEUE_NAME  "/process_queues"
#define MAX_SIZE    1024

//open a queue and send the processes you want to create and the scheduler to schedule them,when you are tired of typing useless things send "shutdown" in order to shutdown the os 
void * queueClient(void * args) {	
  mqd_t mq;
  char buffer[MAX_SIZE];
  char receive_buffer[MAX_SIZE];
  
  mq = mq_open(QUEUE_NAME, O_RDWR); 
  
  //type programs first 
  printf("Type programs you want to run: range from p1 - p10 \nType shutdown in order to turn off the os\n \n"); 
  printf("Type the scheduler you want to use : fifo,sjf,rr,mfq \n \n");
  while(1){
	while(1) {
		char messageToSend[20]; 
		scanf("%s", messageToSend);
		printf("message %s \n",messageToSend);
		snprintf(buffer, sizeof(buffer), "%s", messageToSend); 
		printf("CLIENT: Send message... \n");
		mq_send(mq, buffer, MAX_SIZE, 0);
		char shutdown[] = "shutdown";
		if(!strcmp(messageToSend,shutdown)){
			printf("EXITING \n \n");
			fflush(stdout);
			break;
		}
		
		if(!strcmp(messageToSend,FIFO)){
			break;
		}
		if(!strcmp(messageToSend,SJF)){
			break;
		}
		if(!strcmp(messageToSend,RR)){
			break;
		}
		if(!strcmp(messageToSend,MFQ)){
			break;
		}
		fflush(stdout);
	}
	printf("please do not type until all processes exit \n \n");
	memset(receive_buffer, 0x00, sizeof(receive_buffer)); 
	ssize_t bytes = mq_receive(mq, receive_buffer, 1024, NULL);
	if( bytes >= 0){
		char end[20] = "terminated";
		if(!strcmp(receive_buffer,end)){
			printf("processes are terminated,type shutdown to turn off the system or type 'reset' if you want to check a new scheduler type \n \n");
			char reset[10] = "reset";
			char shutdown[] = "shutdown";
			char message[10];
			scanf("%s",message);
			if(!strcmp(message,reset)){
				snprintf(buffer, sizeof(buffer), "%s",reset); 
				mq_send(mq, buffer, MAX_SIZE, 0);
			} else if(!strcmp(message,shutdown)){
				printf("EXITING \n \n");
				snprintf(buffer, sizeof(buffer), "%s",shutdown); 
				mq_send(mq, buffer, MAX_SIZE, 0);
				exit(0);
			}
		}
	}
 }
  
  mq_close(mq);
  return NULL;
}

int main(int argc, char** argv) {

  pthread_t client;
  printf("Start...\n"); 
  pthread_create(&client, NULL, &queueClient, NULL); //create + start  
  pthread_join(client, NULL);  
  return (EXIT_SUCCESS);
}
