Multi-cpu scheduler


Le modifiche per adattare il sorgente ad un multi cpu constano di :


-3 ready queues : in particolare se si sceglie di usare uno scheduler fifo/shortest job first/round robin verrà utilizzata soltanto la coda 0. Le restanti due code avranno il proprio ruolo solo nel caso di uno scheduler multilevel feedback queue. Il ruolo delle 3 code è servire le 4 cpu ai processi che si trovano nello stato ready, l'algoritmo di scheduling che sceglie il/i processo/i da dispatchare viene determinato prima dell'esecuzione dei processi stessi. 

-Device queues(waiting queues), una per ogni dispositivo IO

-verifica ciclica sulla terminazione di eventi IO per ogni device

-gestione processi attualmente nelle cpu

-implementazione 4 scheduler CPUs : 
    
    - Scheduler FirstComeFirstServed(not preemptive): schedula processi ready sulle 4 CPU in modalità FIFO ;
    
    - Scheduler ShortestJobFirst(preemptive): i processi che gireranno sulle CPU saranno quelli col minor cpu burst disponibile,se un processo torna ready e possiede un cpu burst inferiore ad almeno uno dei processi in cpu(ad esempio in cpu 1),allora questo verrà reinserito in coda ready e il primo processo verrà dispatchato in cpu(cpu 1);
    
    - Scheduler RoundRobin(preemptive): i processi in CPU potranno trattenerla per un tempo massimo pari al quanto di tempo prescelto,processi in coda 0 avranno un cpu quantum pari a 5. Cpu burst con durata maggiore di 5 ritorneranno in coda ready;
    
    - Scheduler MultilevelFeedbackQueue(mfq) : processi verranno schedulati in base alla coda ready in cui si trovano,ogni coda viene gestita in modalità round robin.

-CPUs omogenee : i processori sono identici tra loro in quanto a funzionalità.
      
      
Per quanto riguardo l'affinità per una determinata cpu da parte di un processo, il sistema supporta una soft affinity,dunque il sistema operativo non garantisce che un processo possa rimanere staticamente nella stessa CPU per tutto il suo ciclo di vita.
L'implementazione di soft affinity include dunque migrazione,non viene implementato un sistema di load balancing(push-pull migration) in quanto le code sono condivise dai 4 processori.

Nello scheduler shortest job first non viene implementata politica di aging, questo perchè i cpu burst dei processi sono brevi consentendo a qualunque processo di ottenere cpu in tempi consoni.



L'implementazione consta di un processo client,un processo fake_os, un processo sched_sim. 
Client e sched_sim implementano una politica di message passing tramite message queue, l'utente digita i programmi e la policy di scheduling nella shell di client,sched_sim leggerà i programmi e creerà i relativi processo tramite la funzione FakeProcess_load(definito in fake_process.c).Un processo in questo caso è un insieme di eventi IO/CPU burst,questi verranno inseriti nella lista di eventi associata a esso. Sched_sim inoltre leggerà il tipo di scheduler da attuare,settando l'apposita variabile di fake_os,scheduler.
Dopo aver creato almeno un set di processi e settato la variabile scheduler,sched_sim invocherà la funzione simStep,implementata in fake_os.c,la quale simulerà le istruzioni che un OS esegue.Il sistema operativo dapprima verifica se un nuovo processo è stato caricato,se esiste viene creato il PCB corrispondente al processo; il processo verrà, dunque, inserito nella coda ready 0,oppure nella device queue del dispositivo da cui deve fare I/O, a seconda del tipo di evento in cima alla lista degli eventi del processo.
Fatto ciò, sim_step prosegue verificando se i processi nelle varie device queues hanno terminato i rispettivi I/O.Nel caso, vengono spostati in accordo col prossimo evento nella lista degli eventi,se non esiste, il processo termina e l'area di memoria riservata a esso viene liberata. Dopo il check delle device queues,si verificano i cpu burst dei processi running nelle 4 cpu(dettagli sono inseriti come commenti nel codice).

Sched_sim inoltre implementa le funzioni di scheduling(che fungono sia da scheduler che da dispatcher):

-FCFS : classico algoritmo FIFO,il primo processo a entrare sarà il primo a uscire.
-SJF : il sistema operativo chiamerà questo scheduler ad ogni iterazione affinchè esso verifichi la presenza di un processo a più alta priorità rispetto a quelli attualmente running.La priorità in questo caso è il cpu burst.

-RR : scheduler round robin con quanto di tempo  pari a 5, ciò significa che ogni processo non aspetterà più di (n-1)x5/4(n = numero processi in coda) quanti di cpu prima di ottenerla,evitando qualunque forma di aging.

-MultilevelFeedbackQueue(MFQ) : scheduler che utilizza le 2 code rimanenti, in questo schema le code di livello più alto hanno maggiore priorità rispetto a quelle in basso. Quindi un processo appena creato,non sapendo come si comporterà,verrà inserito nella coda 0;se uno dei processi running veniva da una coda a più bassa priorità,lo scheduler interverrebbe facendo un context switch a favore del nuovo processo.Se quest'ultimo userà la cpu per un tempo maggiore del quanto riservato ai processi provenienti dalla coda 0,verrà degradato ed inserito nella coda di livello immediatamente precedente.
Questo porta chiaramente a problemi di starvation di un processo, per evitare situazioni sgradevoli si attua una politica di aging,ogni processo ha un counter pari a 10,passate 10 iterazioni in una determinata coda,il processo verrà aumentato di grado,spostandolo nella coda immediatamente successiva,aumentando di conseguenza la prioirtà al processo. Ogni coda verrà gestita in modalità round robin, e ognuna di essa avrà un quanto di tempo specifico che aumenta in funzione del livello della coda.

Come eseguire il programma?
Basta eseguire il makefile, aprire due terminali, uno eseguirà il client,l'altro sched_sim.
Client legge i programmi che si vuole caricare e lo scheduler da attuare,sched_sim li eseguirà.
Al termine è possibile o resettare lo scheduler(digitando reset),quindi potremo ricaricare un nuovo set di processi e verificare, se si vuole, un nuovo scheduler, oppure uscire dal processo digitando shutdown.Una volta digitato shutdown nel file Results.txt vengono riportati i tempi di turnaround waiting e response dei processi eseguiti

Vengono forniti i GANTT chart dei scheduler SJF e MFQ per i primi 10 processi 
