#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <unistd.h>
#include "fake_os.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#define FIFO "fifo"
#define SJF "sjf"
#define RR "rr"
#define MFQ "mfq"


//initialize OS data structure
void FakeOS_init(FakeOS* os) {
	os->running_one=0;
	os->running_two = 0;
	os->running_three = 0;
	os->running_four = 0;
	List_init(&os->queue[0]);
	List_init(&os->queue[1]);
	List_init(&os->queue[2]);
	List_init(&os->disk_one_queue);
	List_init(&os->disk_two_queue);
	List_init(&os->camera_queue);
	List_init(&os->processes);
	os->timer=0;
	os->pid_counter = 11;
	os->schedule_fcfs=0;
	os->schedule_sjf= 0;
	os->schedule_rr=0;
	os->schedule_mfq= 0;
	printf("++++++++++++++++++++++++++++++OS initialized+++++++++++++++++++++++++++++++++++++++++++++ \n \n");
}

//here we create a FakePCB corresponding to the FakeProcess made from the p1.txt file(or w/e the file name)
void FakeOS_createProcess(FakeOS* os, FakeProcess* p) {
	// sanity check
	if(os->pid_counter == 100){
		printf("can't handle more than 100 processes \n \n");
		return ;
	}
	if(p->arrival_time < os->timer){
		if(os->pids[p->pid]){
			//process pid already taken,a fork might have been called
			printf("a process with this pid exist or ended, updating pid and arrival time\n \n");
			p->arrival_time = os->timer;
			p->pid = os->pid_counter;
			os->pid_counter++;
		} else {
			//arrival time written in the text file is lower than the actual os time,just update it with the proper value
			printf("process came later than expected updating arrival time \n \n");
			p->arrival_time = os->timer;
		
		}
	}else if(p->arrival_time == os->timer){
		if(os->pids[p->pid]){
			//process pid already taken
			printf("duplicated process %d updating pid \n \n",p->pid);
			p->pid = os->pid_counter;
			os->pid_counter++;
		}
		
	}
	printf("ok\n \n");
	// we check that in the list of PCBs there is no
	// pcb having the same pid
	printf("checking if process is already running  \n \n");
	assert( (!os->running_one || os->running_one->pid!=p->pid) && "pid taken");
	assert( (!os->running_two || os->running_two->pid!=p->pid) && "pid taken");
	assert( (!os->running_three || os->running_three->pid!=p->pid) && "pid taken");
	assert( (!os->running_four || os->running_four->pid!=p->pid) && "pid taken");
	printf("ok process not running \n \n");

  
	//check whether the process already exist in one of the queues (device and ready)
	printf("checking if process is inside ready queues \n");

	ListItem* aux=os->queue[0].first;
	while(aux){
		FakePCB* pcb=(FakePCB*)aux;
		assert(pcb->pid!=p->pid && "pid taken");
		aux=aux->next;
	}	  
  
	printf ("ok process not in this queue \n \n");
 
	printf("checking if process is inside ready queue 2 \n \n");

	aux=os->queue[1].first;
	while(aux){
		FakePCB* pcb=(FakePCB*)aux;
		assert(pcb->pid!=p->pid && "pid taken");
		aux=aux->next;
	}
  
	printf ("ok process not in this queue \n \n");

  
	printf("checking if process is inside ready queue 3\n \n");

	aux=os->queue[2].first;
	while(aux){
		FakePCB* pcb=(FakePCB*)aux;
		assert(pcb->pid!=p->pid && "pid taken");
		aux=aux->next;
	}	  
  
	printf ("ok process not in this queue \n \n");
  
	printf("checking if process is waiting for DISK1 IO operation \n \n");

	aux=os->disk_one_queue.first;
	while(aux){
		FakePCB* pcb=(FakePCB*)aux;
		assert(pcb->pid!=p->pid && "pid taken");
		aux=aux->next;
	}
  
	printf ("ok process not in this queue \n \n");

	printf("checking if process is waiting for DISK2 IO operation \n \n");

	aux=os->disk_two_queue.first;
	while(aux){
		FakePCB* pcb=(FakePCB*)aux;
		assert(pcb->pid!=p->pid && "pid taken");
		aux=aux->next;
	}
 
	printf ("ok process not in this queue \n \n");

	printf("checking if process is waiting for CAMERA IO operation \n \n");

	aux=os->camera_queue.first;
	while(aux){
		FakePCB* pcb=(FakePCB*)aux;
		assert(pcb->pid!=p->pid && "pid taken");
		aux=aux->next;
	}  
  
	printf ("ok process not in this queue \n \n");
  
	printf("everything fine, process control block can finally be created \n \n");

	//end of checking
	// all fine, no such pcb exists
	FakePCB* new_pcb=(FakePCB*) malloc(sizeof(FakePCB));
	new_pcb->list.next=new_pcb->list.prev=0;
	new_pcb->pid=p->pid;
	new_pcb->events=p->events; 
	new_pcb->to_downgrade = 0;
	
	new_pcb->last_task_IO = 0;
	
	new_pcb->just_moved = 0;
	new_pcb->turnaround_time = 0;
	new_pcb->response_time = 0;
	new_pcb->waiting_time = 0;
	if(!strcmp(os->scheduler,MFQ)){
		printf("setting queue number and aging value \n \n");
		//new processes by default in queue 0
		new_pcb->queue = 0;
		new_pcb->aging_aux = 5; //after 10 iterations in which the process is ready in a queue,it gonna be upgraded to the upper level queue,increasing its priority
	}
  
	printf("pcb %d created \n \n",new_pcb->pid);
	os->pids[new_pcb->pid] = new_pcb->pid;//updating pids array with the new one created
	assert(new_pcb->events.first && "process without events");
  
	printf("moving process to the proper queue \n \n");

	// depending on the type of the first event
	// we put the process either in ready or in a device queue
	ProcessEvent* e=(ProcessEvent*)new_pcb->events.first;
	switch(e->type){
		case CPU:
			List_pushBack(&os->queue[0],(ListItem*)new_pcb);
			printf("moved to ready queue \n \n");
			break;
		case DISK1:
			List_pushBack(&os->disk_one_queue, (ListItem*) new_pcb); 
			printf("moved to disk_one_queue \n \n");
			break;
		case DISK2:
			List_pushBack(&os->disk_two_queue, (ListItem*) new_pcb); 
			printf("moved to disk_two_queue \n \n");
			break;
		case CAMERA :
			printf("moved to camera_queue \n \n");
			List_pushBack(&os->camera_queue, (ListItem*) new_pcb); 
			break;
		default:
			assert(0 && "illegal resource");
			;
		}
	
	printf("process created and moved to the proper queue \n \n");
}
void FakeOS_simStep(FakeOS* os,FILE* fp){
  
	
	printf("++++++++++++++++++++++++++++++TIME: %08d+++++++++++++++++++++++++++++++++++++++++++++  \n \n", os->timer);

	
	//scan process waiting to be started
	//and create all processes starting now
	printf("scanning list of processes in order to find new process \n \n");
	ListItem* aux=os->processes.first; 
	while (aux){
		FakeProcess* proc=(FakeProcess*)aux;
		FakeProcess* new_process=0;
		if (proc->arrival_time<=os->timer){
		  printf("a potential new process(process %d) appeared at time %d \n \n",proc->pid,os->timer);
		  new_process=proc;
		}
		aux=aux->next;
		if (new_process) {
		  printf("create pid:%d \n \n", new_process->pid);
		  new_process=(FakeProcess*)List_detach(&os->processes, (ListItem*)new_process);
		  FakeOS_createProcess(os, new_process);
		  free(new_process);
		}
	}
	//checking if processes in device queues ended their IO burst
  
	/* +++++++++++++++++++++++++++++++++++++ DISK1 QUEUE MANAGING ++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
	printf("++++++++++++++++++++++++++++++DISK1 QUEUE MANAGING+++++++++++++++++++++++++++++++++++++++++++++  \n \n");
	aux=os->disk_one_queue.first;
	while(aux) {
		FakePCB* pcb=(FakePCB*)aux;
		aux=aux->next;
		ProcessEvent* e=(ProcessEvent*) pcb->events.first;
		printf("waiting pid: %d \n \n", pcb->pid);
		assert(e->type==DISK1);
		e->duration--;
		pcb->turnaround_time++; //increasing process turnaround time
		printf("remaining time:%d \n \n",e->duration);
		if (e->duration==0){
			printf(" event over \n \n");
			List_popFront(&pcb->events); //remove event e from pcb event list
			free(e); //freeing memory allocated for that event
			List_detach(&os->disk_one_queue, (ListItem*)pcb); //process no more waiting
			if (! pcb->events.first) { 
				// kill process
				printf("process has no event remaining \n \n");
				printf("end process \n \n");
				int pid_end = pcb->pid;
				char mex[] = " terminated,turnaround time,response time and waiting time are rispectively:";
				fprintf(fp,"%d %s",pid_end,mex);
				int t_time = pcb->turnaround_time;
				int r_time = pcb->response_time;
				int w_time = pcb->waiting_time;
				fprintf(fp,"%d , %d , %d \n",t_time,r_time,w_time);
				//write response time, etc on result file
				free(pcb);
			} else {
				//handle next event
				printf("checking next event \n \n");
				e=(ProcessEvent*) pcb->events.first;
				switch (e->type){
				//if next event is a cpu burst then we need to put our process in one of the ready queues
					case CPU:
						pcb->last_task_IO = 1;//response time increased by the next iteration of FakeOS_simStep(if process still in ready queue)
						pcb->just_moved = 1; //this flag is used to increase turnaround time, when we move process out of device queue or cpu we dont want to further increase 														 //turnaround_time,if just_moved is set to 1 then we ignore turnaround_time and set just_moved to 0 (see update waiting/response time 													//section) 
						if(!strcmp(os->scheduler,MFQ)){
							switch(pcb->queue){
								case 0 :
									//in device queues we dont manage downgrades because the last cpu burst was equal or lower than quantum of the queue it was pulled off
									List_pushBack(&os->queue[0],(ListItem*)pcb);
									printf("moved to ready queue 0 \n \n");
									break;
								case 1 :
									List_pushBack(&os->queue[1],(ListItem*)pcb);
									printf("moved to ready queue 1\n \n");
									break;
								case 2 :
									List_pushBack(&os->queue[2],(ListItem*)pcb);
									printf("moved to ready queue 2 \n \n");
									break;
							}
						} else {
							List_pushBack(&os->queue[0],(ListItem*)pcb);
							printf("moved to ready queue 0 \n \n");
							break;
						}
									
				break;
				case DISK1:
			  		printf("move to disk_one_queue \n \n");
					List_pushBack(&os->disk_one_queue, (ListItem*) pcb);
					break;
				case DISK2 :
					printf("move to disk_two_queue \n \n");
					List_pushBack(&os->disk_two_queue,(ListItem*)pcb);
					break;
				case CAMERA :
					printf("move to camera queue \n \n");
					List_pushBack(&os->camera_queue,(ListItem*)pcb);
					break;			
			}
		  }
		}
	}
	printf("++++++++++++++++++++++++++++++DISK1 QUEUE MANAGING ENDED+++++++++++++++++++++++++++++++++++++++++++++  \n \n");

    /* +++++++++++++++++++++++++++++++++++++ DISK2 QUEUE MANAGING ++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

	printf("++++++++++++++++++++++++++++++DISK2 QUEUE MANAGING+++++++++++++++++++++++++++++++++++++++++++++  \n \n");
	//same stuff as disk1 managing
	aux=os->disk_two_queue.first;
	while(aux) {
		FakePCB* pcb=(FakePCB*)aux;
		aux=aux->next;
		ProcessEvent* e=(ProcessEvent*) pcb->events.first;
		printf("waiting pid: %d \n \n", pcb->pid);
		assert(e->type==DISK2);
		e->duration--;
		pcb->turnaround_time++;
		printf("remaining time:%d \n \n",e->duration);
		if (e->duration==0){
			printf("event over  \n \n");
			List_popFront(&pcb->events); 
			free(e); 
			List_detach(&os->disk_two_queue, (ListItem*)pcb);
			if (! pcb->events.first) {
			// kill process
				printf("process has no event remaining \n \n ");
				printf("end process\n \n");
				int pid_end = pcb->pid;
				char mex[] = " terminated,turnaround time,response time and waiting time are rispectively:";
				fprintf(fp,"%d %s",pid_end,mex);
				int t_time = pcb->turnaround_time;
				int r_time = pcb->response_time;
				int w_time = pcb->waiting_time;
				fprintf(fp,"%d , %d, %d \n",t_time,r_time,w_time);
				free(pcb);
			} else {
				//handle next event
				printf("checking next event \n \n");
				e=(ProcessEvent*) pcb->events.first;
				switch (e->type){
					case CPU:
						pcb->last_task_IO = 1;
						pcb->just_moved = 1;
						if(!strcmp(os->scheduler,MFQ)){
							switch(pcb->queue){
								case 0 :
									List_pushBack(&os->queue[0],(ListItem*)pcb);
									printf("moved to ready queue 0 \n \n");
									break;
								case 1 :
									List_pushBack(&os->queue[1],(ListItem*)pcb);
									printf("moved to ready queue 1\n \n");
									break;
								case 2 :
									List_pushBack(&os->queue[2],(ListItem*)pcb);
									printf("moved to ready queue 2 \n \n");
									break;
							}
						} else {
							List_pushBack(&os->queue[0],(ListItem*)pcb);
							printf("moved to ready queue 0 \n \n");
							break;
						}
					break;
					case DISK1:
						printf("move to disk_one_queue \n \n");
						List_pushBack(&os->disk_one_queue, (ListItem*) pcb);
						break;
					case DISK2 :
						printf("move to disk_two_queue \n \n");
						List_pushBack(&os->disk_two_queue,(ListItem*)pcb);
						break;
					case CAMERA :
						printf("move to camera queue \n \n");
						List_pushBack(&os->camera_queue,(ListItem*)pcb);
						break;			
				}
			}
		}
	}
	printf("++++++++++++++++++++++++++++++DISK2 QUEUE MANAGING ENDED+++++++++++++++++++++++++++++++++++++++++++++  \n \n");

	/* +++++++++++++++++++++++++++++++++++++ CAMERA QUEUE MANAGING ++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

	printf("++++++++++++++++++++++++++++++CAMERA QUEUE MANAGING+++++++++++++++++++++++++++++++++++++++++++++   \n \n");
	aux=os->camera_queue.first;
	while(aux) {
		FakePCB* pcb=(FakePCB*)aux;
		aux=aux->next;
		ProcessEvent* e=(ProcessEvent*) pcb->events.first;
		printf("waiting pid: %d\n \n", pcb->pid);
		assert(e->type==CAMERA);
		e->duration--;
		pcb->turnaround_time++;
		printf("remaining time:%d\n \n",e->duration);
		if (e->duration==0){
			printf("event over \n \n");
			List_popFront(&pcb->events); 
			free(e); 
			List_detach(&os->camera_queue, (ListItem*)pcb); 
			if(!pcb->events.first){
				// kill process
				printf("process has no event remaining \n \n");
				printf("end process\n \n");
				int pid_end = pcb->pid;
				char mex[] = " terminated,turnaround time,response time and waiting time are rispectively:";
				fprintf(fp,"%d %s",pid_end,mex);
				int t_time = pcb->turnaround_time;
				int r_time = pcb->response_time;
				int w_time = pcb->waiting_time;
				fprintf(fp,"%d, %d, %d \n", t_time,r_time,w_time);
				free(pcb);
			} else {
				//handle next event
				printf("checking next event \n \n");
				e=(ProcessEvent*) pcb->events.first;
				switch (e->type){
					case CPU:
						
						pcb->just_moved = 1;
						pcb->last_task_IO = 1;
						if(!strcmp(os->scheduler,MFQ)){
							switch(pcb->queue){
								case 0 :
									List_pushBack(&os->queue[0],(ListItem*)pcb);
									printf("moved to ready queue 0 \n \n");
									break;
								case 1 :
									List_pushBack(&os->queue[1],(ListItem*)pcb);
									printf("moved to ready queue 1\n \n");
									break;
								case 2 :
									List_pushBack(&os->queue[2],(ListItem*)pcb);
									printf("moved to ready queue 2 \n \n");
									break;
							}
						} else {
							List_pushBack(&os->queue[0],(ListItem*)pcb);
							printf("moved to ready queue 0 \n \n");
							break;
						}
					break;
					case DISK1:
						printf("move to disk_one_queue \n \n");
						List_pushBack(&os->disk_one_queue, (ListItem*) pcb);
						break;
					case DISK2 :
						printf("move to disk_two_queue \n \n");
						List_pushBack(&os->disk_two_queue,(ListItem*)pcb);
						break;
					case CAMERA :
						printf("move to camera queue \n \n");
						List_pushBack(&os->camera_queue,(ListItem*)pcb);
						break;			
				}
			}
		}
	}
	printf("++++++++++++++++++++++++++++++CAMERA QUEUE MANAGING ENDED +++++++++++++++++++++++++++++++++++++++++++++ \n \n");
  //end of IO checks

  

  
  
  //check cpu burst of running processes
  
  /* +++++++++++++++++++++++++++++++++++++ CPU 1 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
	
	printf("++++++++++++++++++++++++++++++HANDLING CPU 1+++++++++++++++++++++++++++++++++++++++++++++ \n \n");
	
	FakePCB* runningOne = os->running_one;
	printf("running pid: %d \n \n", runningOne ? runningOne->pid:-1);
	if (runningOne) {
		ProcessEvent* e=(ProcessEvent*) runningOne->events.first;
		assert(e->type==CPU);
		runningOne->turnaround_time++;
		e->duration--;
		printf("remaining time:%d\n \n",e->duration);
		if (e->duration==0){ 
			printf("event over \n \n");
			List_popFront(&runningOne->events);
			free(e);
			if (! runningOne->events.first) {
				printf("process has no event remaining \n \n");
				printf("end process\n \n");
				int pid_end = runningOne->pid;
				char mex[] = " terminated,turnaround time,response time and waiting time are rispectively:" ;
				fprintf(fp,"%d %s",pid_end,mex);
				int t_time = runningOne->turnaround_time;
				int r_time = runningOne->response_time;
				int w_time = runningOne->waiting_time;
				fprintf(fp,"%d, %d, %d \n",t_time,r_time,w_time);
				free(runningOne);
			} else {
				printf("checking next event \n \n");
				e=(ProcessEvent*) runningOne->events.first;
				switch (e->type){
					case CPU:
						if(!strcmp(os->scheduler,MFQ)){
							switch(runningOne->queue){
								case 0 :
									if ( runningOne->to_downgrade){ //bad process,its cpu burst was higher than queue 0 quantum
										runningOne->aging_aux = 5; //when downgrading/upgrading,reset aging value
										runningOne->to_downgrade = 0;
										runningOne->queue = 1;
										runningOne->just_moved = 1;
										List_pushBack(&os->queue[1],(ListItem*)runningOne);
										printf("process took too long,downgrading it to next lower queue \n \n");
									} else {
										runningOne->just_moved = 1;
										List_pushBack(&os->queue[0],(ListItem*)runningOne);//good process,cpu burst was equal or lower than queue 0 quantum 
										printf("move to queue 0 \n \n");
									}
									break;
								case 1 :
									if ( runningOne->to_downgrade){ //bad process,its cpu burst was higher than queue 1 quantum
										runningOne->aging_aux = 5;
										runningOne->to_downgrade = 0;
										runningOne->queue = 2;
										
										runningOne->just_moved = 1;
										List_pushBack(&os->queue[2],(ListItem*)runningOne);
										printf("process took too long,downgrading it to next lower queue \n \n");
									} else {
										
										runningOne->just_moved = 1;
										List_pushBack(&os->queue[1],(ListItem*)runningOne);
										printf("move to queue 1 \n \n");
									}					
									break;
								case 2 :
								
									runningOne->just_moved = 1;
									List_pushBack(&os->queue[2],(ListItem*)runningOne); //cant downgrade more a process
									printf("move to queue 2 \n \n");
									break;
							}
						} else {
						
							runningOne->just_moved = 1;
							List_pushBack(&os->queue[0],(ListItem*)runningOne);
							printf("moved to ready queue 0 \n \n");
							break;
						}
						break;		
					case DISK1:
						printf("move to disk_one_queue \n \n");
						List_pushBack(&os->disk_one_queue, (ListItem*) runningOne);
						break;
					case DISK2 :
						printf("move to disk_two_queue \n \n");
						List_pushBack(&os->disk_two_queue,(ListItem*)runningOne);
						break;
					case CAMERA :
						printf("move to camera queue \n \n");
						List_pushBack(&os->camera_queue,(ListItem*)runningOne);
						break;
				}
			}
			os->running_one = 0; 
		}
	}
	
	
	
	printf("++++++++++++++++++++++++++++++HANDLING CPU 1 DONE+++++++++++++++++++++++++++++++++++++++++++++  \n \n");
	
	/* +++++++++++++++++++++++++++++++++++++ CPU 2 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
	
	printf("++++++++++++++++++++++++++++++HANDLING CPU 2+++++++++++++++++++++++++++++++++++++++++++++ \n \n");
	
	
	FakePCB* runningTwo = os->running_two;
	printf("running pid: %d \n \n", runningTwo?runningTwo->pid:-1);
	if (runningTwo) {
		ProcessEvent* e=(ProcessEvent*) runningTwo->events.first;
		assert(e->type==CPU);
		runningTwo->turnaround_time++;
		e->duration--;
		printf("remaining time:%d \n \n",e->duration);
		if (e->duration==0){ 
			printf("event over \n \n");
			List_popFront(&runningTwo->events);
			free(e);
			if (! runningTwo->events.first) {
				printf("process has no event remaining \n \n ");
				printf("end process\n \n");
				int pid_end = runningTwo->pid;
				char mex[] = " terminated,turnaround time,response time and waiting time are rispectively:";
				fprintf(fp,"%d %s",pid_end,mex);
				int t_time = runningTwo->turnaround_time;
				int r_time = runningTwo->response_time;
				int w_time = runningTwo->waiting_time;
				fprintf(fp,"%d, %d, %d \n",t_time,r_time,w_time);
				free(runningTwo);
			} else {
				printf("checking next event \n \n");
				e=(ProcessEvent*) runningTwo->events.first;
				switch (e->type){
					case CPU:
						if(!strcmp(os->scheduler,MFQ)){
							switch(runningTwo->queue){
								case 0 :
									if ( runningTwo->to_downgrade){ //bad process,its cpu burst was higher than queue 0 quantum
										runningTwo->aging_aux = 5; //when downgrading/upgrading,reset aging value
										runningTwo->to_downgrade = 0;
										runningTwo->queue = 1;
										
										runningTwo->just_moved = 1;
										List_pushBack(&os->queue[1],(ListItem*)runningTwo);
										printf("process took too long,downgrading it to next lower queue \n \n");
									} else {
										
										runningTwo->just_moved = 1;
										List_pushBack(&os->queue[0],(ListItem*)runningTwo);
										printf("move to queue 0 \n \n");
									}
									break;
								case 1 :
									if ( runningTwo->to_downgrade){ //bad process,its cpu burst was higher than queue 1 quantum
										runningTwo->aging_aux = 5;
										runningTwo->to_downgrade = 0;
										runningTwo->queue = 2;
								
										runningTwo->just_moved = 1;
										List_pushBack(&os->queue[2],(ListItem*)runningTwo);
										printf("process took too long,downgrading it to next lower queue \n \n");
									} else {
									
										runningTwo->just_moved = 1;
										List_pushBack(&os->queue[1],(ListItem*)runningTwo);
										printf("move to queue 1 \n \n");
									}					
									break;
								case 2 :
									
									runningTwo->just_moved = 1;
									List_pushBack(&os->queue[2],(ListItem*)runningTwo); //cant downgrade more a process
									printf("move to queue 2 \n \n");
									break;
							}
						} else {
						
							runningTwo->just_moved = 1;
							List_pushBack(&os->queue[0],(ListItem*)runningTwo);
							printf("moved to ready queue 0 \n \n");
							break;
						}
						break;
					case DISK1:
						List_pushBack(&os->disk_one_queue, (ListItem*) runningTwo);
						printf("move to disk_one_queue\n \n");
						break;
					case DISK2 :
						List_pushBack(&os->disk_two_queue,(ListItem*)runningTwo);
						printf("move to disk_two_queue\n \n");
						break;
					case CAMERA :
						List_pushBack(&os->camera_queue,(ListItem*)runningTwo);
						printf("move to camera_queue\n \n");
						break;
				}
			}
			os->running_two = 0; 
		}
	}  
	

	
	printf("++++++++++++++++++++++++++++++HANDLING CPU 2 DONE+++++++++++++++++++++++++++++++++++++++++++++  \n \n");
	
	/* +++++++++++++++++++++++++++++++++++++ CPU 3 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
	
	printf("++++++++++++++++++++++++++++++HANDLING CPU 3 +++++++++++++++++++++++++++++++++++++++++++++ \n \n");
	
	FakePCB* runningThree =os->running_three;
	printf("running pid: %d \n \n", runningThree?runningThree->pid:-1);
	if (runningThree) {
		ProcessEvent* e=(ProcessEvent*) runningThree->events.first;
		assert(e->type==CPU);
		runningThree->turnaround_time++;
		e->duration--;
		printf("remaining time:%d \n \n",e->duration);
		if (e->duration==0){
			printf("event over \n \n");
			List_popFront(&runningThree->events);
			free(e);
			if (! runningThree->events.first) {
				printf("process has no event remaining \n \n");
				printf("end process\n \n");
				int pid_end = runningThree->pid;
				char mex[] = " terminated,turnaround time,response time and waiting time are rispectively:";
				fprintf(fp,"%d %s",pid_end,mex);
				int t_time = runningThree->turnaround_time;
				int r_time = runningThree->response_time;
				int w_time = runningThree->waiting_time;
				fprintf(fp,"%d, %d, %d \n",t_time,r_time,w_time);
				free(runningThree); 
			} else {
				printf("checking next event \n \n");
				e=(ProcessEvent*) runningThree->events.first;
				switch (e->type){
					case CPU:
						if(!strcmp(os->scheduler,MFQ)){
							switch(runningThree->queue){
								case 0 :
									if ( runningThree->to_downgrade){ //bad process,its cpu burst was higher than queue 0 quantum
										runningThree->aging_aux = 5; //when downgrading/upgrading,reset aging value
										runningThree->to_downgrade = 0;
										runningThree->queue = 1;
										
										runningThree->just_moved = 1;
										List_pushBack(&os->queue[1],(ListItem*)runningThree);
										printf("process took too long,downgrading it to next lower queue \n \n");
									} else {
									
										runningThree->just_moved = 1;
										List_pushBack(&os->queue[0],(ListItem*)runningThree);
										printf("move to queue 0 \n \n");
									}
									break;
								case 1 :
									if ( runningThree->to_downgrade){ //bad process,its cpu burst was higher than queue 1 quantum
										runningThree->aging_aux = 5;
										runningThree->to_downgrade = 0;
										runningThree->queue = 2;
									
										runningThree->just_moved = 1;
										List_pushBack(&os->queue[2],(ListItem*)runningThree);
										printf("process took too long,downgrading it to next lower queue \n \n");
									} else {
										
										runningThree->just_moved = 1;
										List_pushBack(&os->queue[1],(ListItem*)runningThree);
										printf("move to queue 1 \n \n");
									}					
									break;
								case 2 :
									
									runningThree->just_moved = 1;
									List_pushBack(&os->queue[2],(ListItem*)runningThree); //cant downgrade more a process
									printf("move to queue 2 \n \n");
									break;
							}
						} else {
						
							runningThree->just_moved = 1;
							List_pushBack(&os->queue[0],(ListItem*)runningThree);
							printf("moved to ready queue 0 \n \n");
							break;
						}
						break;
					case DISK1:
						List_pushBack(&os->disk_one_queue, (ListItem*) runningThree);
						printf("move to disk_one_queue \n \n");
						break;
					case DISK2 :
						List_pushBack(&os->disk_two_queue,(ListItem*)runningThree);
						printf("move to disk_two_queue \n \n");
						break;
					case CAMERA :
						List_pushBack(&os->camera_queue,(ListItem*)runningThree);
						printf("move to camera queue \n \n");
						break;
				}
			}
			os->running_three = 0; 
		}
	}
	

	
	printf("++++++++++++++++++++++++++++++HANDLING CPU 3 DONE+++++++++++++++++++++++++++++++++++++++++++++  \n \n");
	/* +++++++++++++++++++++++++++++++++++++ CPU 4 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
	
	printf("++++++++++++++++++++++++++++++HANDLING CPU 4 +++++++++++++++++++++++++++++++++++++++++++++ \n \n");
	FakePCB* runningFour =os->running_four;
	printf("running pid: %d \n \n", runningFour?runningFour->pid:-1);
	if (runningFour) {
		ProcessEvent* e=(ProcessEvent*) runningFour->events.first;
		assert(e->type==CPU);
		runningFour->turnaround_time++;
		e->duration--;
		printf("remaining time:%d \n \n",e->duration);
		if (e->duration==0){ 
			printf("event ended \n \n");
			List_popFront(&runningFour->events);
			free(e);
			if (! runningFour->events.first) {
				printf("process has no event remaining \n \n");
				printf("end process\n \n");
				int pid_end = runningFour->pid;
				char mex[] = " terminated,turnaround time,response time and waiting time are rispectively:";
				fprintf(fp,"%d %s",pid_end,mex);
				int t_time = runningFour->turnaround_time;
				int r_time = runningFour->response_time;
				int w_time = runningFour->waiting_time;
				fprintf(fp,"%d, %d, %d \n",t_time,r_time,w_time);
				free(runningFour); // kill process
			} else {
				printf("checking next event \n \n");
				e=(ProcessEvent*) runningFour->events.first;
				switch (e->type){
					case CPU:
						if(!strcmp(os->scheduler,MFQ)){
							switch(runningFour->queue){
								case 0 :
									if ( runningFour->to_downgrade){ //bad process,its cpu burst was higher than queue 0 quantum
										runningFour->aging_aux = 5; //when downgrading/upgrading,reset aging value
										runningFour->to_downgrade = 0;
										runningFour->queue = 1;
					
										runningFour->just_moved = 1;
										List_pushBack(&os->queue[1],(ListItem*)runningFour);
										printf("process took too long,downgrading it to next lower queue \n \n");
									} else {
										runningFour->just_moved = 1;
									
										List_pushBack(&os->queue[0],(ListItem*)runningFour);
										printf("move to queue 0 \n \n");
									}
									break;
								case 1 :
									if ( runningFour->to_downgrade){ //bad process,its cpu burst was higher than queue 1 quantum
										runningFour->aging_aux = 5;
										runningFour->to_downgrade = 0;
										runningFour->queue = 2;
										
										runningFour->just_moved = 1;
										List_pushBack(&os->queue[2],(ListItem*)runningFour);
										printf("process took too long,downgrading it to next lower queue \n \n");
									} else {
										
										runningFour->just_moved = 1;
										List_pushBack(&os->queue[1],(ListItem*)runningFour);
										printf("move to queue 1 \n \n");
									}					
									break;
								case 2 :
									
									runningFour->just_moved = 1;
									List_pushBack(&os->queue[2],(ListItem*)runningFour); //cant downgrade more a process
									printf("move to queue 2 \n \n");
									break;
							}
						} else {
							
							runningFour->just_moved = 1;
							List_pushBack(&os->queue[0],(ListItem*)runningFour);
							printf("moved to ready queue 0 \n \n");
							break;
						}
						break;
					case DISK1:
						printf("move to disk_one_queue \n \n");
						List_pushBack(&os->disk_one_queue, (ListItem*) runningFour);
						break;
					case DISK2 :
						printf("move to disk_two_queue \n \n");
						List_pushBack(&os->disk_two_queue,(ListItem*)runningFour);
						break;
					case CAMERA :
						printf("move to camera queue \n \n");
						List_pushBack(&os->camera_queue,(ListItem*)runningFour);
						break;
				}
			}
			os->running_four = 0; 
		}
	}
	printf("++++++++++++++++++++++++++++++HANDLING CPU 4 DONE+++++++++++++++++++++++++++++++++++++++++++++  \n \n");
	//update turnaround/waiting/response time
	
	if(os->queue[0].first){
		printf("updating waiting and response time for processes in ready queue 0 \n \n");
		ListItem* item = os->queue[0].first;
		while(item){
			FakePCB* pcb = (FakePCB*)item;
			if(!pcb->just_moved){
				pcb->turnaround_time++;
				pcb->waiting_time++;
				printf("process %d waiting time increased \n \n",pcb->pid);
				if(pcb->last_task_IO){
					//last pcb task was an IO, increasing response time 
					printf("process %d response time increased \n \n",pcb->pid);
					pcb->response_time++;
				}
			
			} else{
				pcb->just_moved = 0; //turnaround time has already been incremented by the os during device/cpu handling,increasing it by the next step
			}
			
				
			
			item = item->next;
		}
	}
	
	if(os->queue[1].first){
		printf("updating waiting and response time for processes in ready queue 1 \n \n");
		ListItem* item = os->queue[1].first;
		while(item){
			FakePCB* pcb = (FakePCB*)item;
			if(!pcb->just_moved){
				pcb->turnaround_time++;
				pcb->waiting_time++;
				printf("process %d waiting time increased \n \n",pcb->pid);
				if(pcb->last_task_IO){
					printf("process %d response time increased \n \n",pcb->pid);
					pcb->response_time++;
				}
			} else{
				pcb->just_moved = 0; 
			}
			
			item = item->next;
		}
	}
	if(os->queue[2].first){
		printf("updating waiting and response time for processes in ready queue 2 \n \n");
		ListItem* item = os->queue[2].first;
		while(item){
			FakePCB* pcb = (FakePCB*)item;
			if(!pcb->just_moved){
				pcb->turnaround_time++;
				pcb->waiting_time++;
				printf("process %d waiting time increased \n \n",pcb->pid);
				if(pcb->last_task_IO){
					printf("process %d response time increased \n \n",pcb->pid);
					pcb->response_time++;
				}
			} else{
				pcb->just_moved = 0; //turnaround time has already been incremented by the os during device/cpu handling
			}			
			item = item->next;
		}
	}
	
	//call scheduler 
	printf("++++++++++++++++++++++++++++++CALLING SCHEDULER+++++++++++++++++++++++++++++++++++++++++++++++  \n \n");
	if(!strcmp(os->scheduler,FIFO)){
		(*os->schedule_fcfs)(os);
	} else if(!strcmp(os->scheduler,SJF)){
		//always call sjf scheduler
		(*os->schedule_sjf)(os);
	} else if (!strcmp(os->scheduler,RR)){
		if(!os->running_one || !os->running_two || !os->running_three || !os->running_four){
			(*os->schedule_rr)(os,os->schedulerr_args);
		}
	} else if(!strcmp(os->scheduler,MFQ)){
		//always call mfq scheduler
		(*os->schedule_mfq)(os,os->schedulemfq_args);
	}
	++os->timer;	
	
}
