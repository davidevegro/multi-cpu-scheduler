
#include <fcntl.h>
#include <mqueue.h> 
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <assert.h>
#include "fake_os.h"

#define QUEUE_NAME  "/process_queues"
#define MAX_SIZE 1024


FakeOS os;

typedef struct {
	int quantum;
} SchedRRArgs;

typedef struct {
	int quantum[3]; //every queue has its Cpu quantum
} SchedMFQArgs;




void * queueServer(void * args) {
	mqd_t mq; 
	FILE * fp;
	struct mq_attr attr;
	char buffer_client[20];
	char buffer[20];
	int terminated = 0;
	/* initialize the queue attributes */
	attr.mq_flags = 0;
	attr.mq_maxmsg = 10;
	attr.mq_msgsize = 1024;
	attr.mq_curmsgs = 0;
	char fifo[] = "fifo";
	char sjf[] = "sjf";
	char rr[] = "rr";
	char mfq[] = "mfq";
  
 
	//we will receive processes to create, exit when shutdown will be read
	mq = mq_open(QUEUE_NAME, O_CREAT | O_RDWR, 0644, &attr);
	while(1) {
		memset(buffer_client, 0x00, sizeof(buffer_client)); 
		ssize_t bytes = mq_receive(mq, buffer_client, 1024, NULL);
		if(bytes >= 0){
			char shutdown[20] = "shutdown";
			if(!strcmp(buffer_client,shutdown)){
				printf("EXITING \n \n");
				fflush(stdout);
				mq_close(mq);
				fclose(fp);
				exit(-1);
			} else {
				char reset[10] = "reset"; //again type processes first and then scheduler function
				if(!strcmp(buffer_client,reset)){
					terminated = 0;
					memset(buffer_client, 0x00, sizeof(buffer_client)); 
					mq_receive(mq, buffer_client, 1024, NULL); //next read needed here(first process from i-th iteration)
				}
			}
		}
		
			//verify if the string received is a scheduler type
			if(!strcmp(buffer_client,fifo)){
				//fifo scheduler choosen
				strcpy(os.scheduler,buffer_client);
				
			}else if(!strcmp(buffer_client,sjf)){
				//sjf scheduler choosen
				strcpy(os.scheduler,buffer_client);
				
			} else if(!strcmp(buffer_client,rr)){
				//rr scheduler choosen
				strcpy(os.scheduler,buffer_client);
				
			}else if(!strcmp(buffer_client,mfq)){
				//mfq scheduler choosen
				strcpy(os.scheduler,buffer_client);
				
			} else {
				printf("in charge : process %s \n \n",buffer_client);
				FakeProcess new_process;
				int num_events=FakeProcess_load(&new_process, buffer_client);
				printf("loading [%s], pid: %d, events:%d \n \n",buffer_client, new_process.pid, num_events);
				if (num_events) {
					FakeProcess* new_process_ptr=(FakeProcess*)malloc(sizeof(FakeProcess));
					*new_process_ptr=new_process;
					List_pushBack(&os.processes, (ListItem*)new_process_ptr); 
				}	
			}
		if(!strcmp(buffer_client,fifo)||!strcmp(buffer_client,sjf)||!strcmp(buffer_client,rr)||!strcmp(buffer_client,mfq)){
			fp = fopen("Results.txt","a+");
			char mex[] = "Results for scheduler ";
			fprintf(fp,"%s %s \n ",mex,buffer_client);
			while(!terminated){
				if(os.running_one|| os.running_two|| os.running_three || os.running_four
				|| os.queue[0].first || os.queue[1].first || os.queue[2].first
				|| os.disk_one_queue.first||os.disk_two_queue.first||os.camera_queue.first
				|| os.processes.first) {
					FakeOS_simStep(&os,fp);
				} else {
					//no processes left 
					char m[] = "*************************** \n";
					fprintf(fp,"%s",m);
					terminated = 1;
					char messageToSend[20] = "terminated"; 
					snprintf(buffer, sizeof(buffer), "%s", messageToSend); 
					mq_send(mq, buffer, MAX_SIZE, 0);
				}
			}	
		}		
	}
}

/* ++++++++++++++++++++++++++++++++++++SCHEDULER FIFO++++++++++++++++++++++++++++++*/
void schedFCFS(FakeOS* os ){ 
	
	printf("SCHEDULER FCFS \n \n ");
	if(!os->queue[0].first){
		//no ready processes,returning
		return;
	}
	if(!os->running_one){
		FakePCB* pcb = (FakePCB*)List_popFront(&os->queue[0]);
		pcb->last_task_IO = 0;
		printf("now running process %d on CPU 1 \n \n",pcb->pid);
		os->running_one = pcb;	
	}
	if(!os->running_two){
		if(!os->queue[0].first){
			//no ready processes,returning
			return;
		}
		FakePCB* pcb = (FakePCB*)List_popFront(&os->queue[0]);
		pcb->last_task_IO = 0;
		printf("now running process %d on CPU 2 \n \n",pcb->pid);
		os->running_two = pcb;	
	}
	if(!os->running_three){
		if(!os->queue[0].first){
			//no ready processes,returning
			return;
		}
		FakePCB* pcb = (FakePCB*)List_popFront(&os->queue[0]);
		pcb->last_task_IO = 0;
		printf("now running process %d on CPU 3 \n \n",pcb->pid);
		os->running_three = pcb;	
	}
	if(!os->running_four){
		if(!os->queue[0].first){
			//no ready processes,returning
			return;
		}
		FakePCB* pcb = (FakePCB*)List_popFront(&os->queue[0]);
		pcb->last_task_IO = 0;
		printf("now running process %d on CPU 4 \n \n",pcb->pid);
		os->running_four = pcb;	
	}		
	printf("DONE \n \n ");
}

/* +++++++++++++++++++++++++++++++++++++ SCHEDULER ShortestJobFirst++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
//support function returning the pcb,inside ready queue 0,with the lowest cpu burst
FakePCB* minCpuBurst(FakeOS* os){
	FakePCB* pcb = 0; //pcb that has to be updated with the lowest cpu burst value process
	int max_cpu_burst = 200; //random high value
	ListItem* aux = os->queue[0].first;
	while(aux){
		FakePCB* pcb_aux = (FakePCB*)aux;
		ProcessEvent* p = (ProcessEvent*) pcb_aux->events.first;
		assert(p->type==CPU);
		if(p->duration < max_cpu_burst){
			max_cpu_burst = p->duration;
			pcb = pcb_aux;
		}
		aux = aux->next;
	}
	
	return pcb;
}
//brute force algorithm,might use merge sort
//sorting cpus by cpu burst duration of their processes running
void sortCPU(FakeOS* os,int ar[],int res[]){
	ProcessEvent* p1 = (ProcessEvent*)os->running_one->events.first;
	int burst_one = p1->duration;
	ProcessEvent* p2 = (ProcessEvent*)os->running_two->events.first;
	int burst_two = p2->duration;
	ProcessEvent* p3 = (ProcessEvent*)os->running_three->events.first;
	int burst_three = p3->duration;
	ProcessEvent* p4 = (ProcessEvent*)os->running_four->events.first;
	int burst_four = p4->duration;
	ar[0] = burst_one ;
	if(burst_two > burst_one){
		ar[0] = burst_two;
		ar[1] = burst_one;
	} else {
		ar[1] = burst_two;
	}
	if(burst_three > ar[0]){
			int aux0 = ar[0];
			int aux1 = ar[1];
			ar[0] = burst_three;
			ar[1] = aux0;
			ar[2] = aux1;
	}else if(burst_three > ar[1]){
		int aux0 = ar[1];
		ar[1] = burst_three;
		ar[2] = aux0;
	} else if(burst_three <= ar[1]){
		ar[2] = burst_three;
	}
	if(burst_four > ar[0]){
		int aux0 = ar[0];
		int aux1 = ar[1];
		int aux2 = ar[2];
		ar[0] = burst_four;
		ar[1] = aux0;
		ar[2] = aux1;
		ar[3] = aux2;
	} else if (burst_four > ar[1]){
		int aux0 = ar[1];
		int aux1 = ar[2];
		ar[1] = burst_four;
		ar[2] = aux0;
		ar[3] = aux1;
	}else if(burst_four > ar[2]){
		int aux = ar[2];
		ar[2] = burst_four;
		ar[3] = aux;
	}else if(burst_four <= ar[2]){
		ar[3] = burst_four;
	}
	int assigned_one = 0;
	int assigned_two = 0;
	int assigned_three = 0;
	int assigned_four = 0;
	for(int i = 0; i < 4; i++){
		if(ar[i] == burst_one && !assigned_one){
			res[i] = 1;
			assigned_one = 1;
		} else if (ar[i] == burst_two && !assigned_two){
			res[i] = 2;
			assigned_two = 1;
		}else if(ar[i] == burst_three && !assigned_three){
			res[i] = 3;
			assigned_three = 1;
		} else if (ar[i] == burst_four && !assigned_four){
			res[i] = 4;
			assigned_four = 1;
		}
	}
	printf("sorting done \n \n");
	
}
	

void schedSJF(FakeOS* os ){ 
	
	printf("SCHEDULER SJF \n \n");
	int just_dispatched_one = 0;
	int just_dispatched_two = 0;
	int just_dispatched_three = 0;
	int just_dispatched_four = 0;
	int cpus[4];
	int res[4];
	if(!os->running_one){
		FakePCB* pcb = minCpuBurst(os); //pcb that has to be updated with the lowest cpu burst value process
		if(pcb){
			FakePCB* pcb1 = (FakePCB*)List_detach(&os->queue[0],(ListItem*)pcb);
			pcb1->last_task_IO = 0;
			os->running_one = pcb1;
			just_dispatched_one = 1; 
			printf("now running process %d on CPU 1 \n \n",pcb1->pid);
		}
	}
	if(!os->running_two){
		FakePCB* pcb = minCpuBurst(os); //pcb that has to be updated with the lowest cpu burst value process
		if(pcb){
			FakePCB* pcb1 = (FakePCB*)List_detach(&os->queue[0],(ListItem*)pcb);
			pcb1->last_task_IO = 0;
			os->running_two = pcb1;
			just_dispatched_two = 1;
			printf("now running process %d on CPU 2 \n \n",pcb1->pid);
		}
	}
	if(!os->running_three){
		FakePCB* pcb = minCpuBurst(os); //pcb that has to be updated with the lowest cpu burst value process
		if(pcb){
			FakePCB* pcb1 = (FakePCB*)List_detach(&os->queue[0],(ListItem*)pcb);
			pcb1->last_task_IO = 0;
			os->running_three = pcb1;
			just_dispatched_three = 1;
			printf("now running process %d on CPU 3 \n \n",pcb1->pid);
		}
	}
	if(!os->running_four){
		FakePCB* pcb = minCpuBurst(os); //pcb that has to be updated with the lowest cpu burst value process
		if(pcb){
			FakePCB* pcb1 = (FakePCB*)List_detach(&os->queue[0],(ListItem*)pcb);
			pcb1->last_task_IO = 0;
			os->running_four = pcb1;
			just_dispatched_four = 1;
			printf("now running process %d on CPU 4 \n \n",pcb1->pid);
		}
	}
	//sort actual_running_cpu_burst,put the index of the cpu inside the array
	if(os->running_one && os->running_two && os->running_three && os->running_four && os->queue[0].first){
		printf("checking if a process in ready queue has a lower cpu burst than one of actual runnings \n \n");
		sortCPU(os,cpus,res);
		//after that, cycle on that array and move processes in queue inside that cpu if its cpu burst is lower than actual_running_cpu_burst(only if just_dispatched value is 0)
		for(int i = 0; i < 4 ;i++){
			if(!os->queue[0].first){
				continue;
			}
			
			switch(res[i]){
				
				case 1 :
					if(just_dispatched_one){ //cpu has been assigned in this iteration,we dont need to check it
						break;
					}
					FakePCB* pcb = minCpuBurst(os); //pcb that has to be updated with the lowest cpu burst value process
					ProcessEvent* proc = (ProcessEvent*) pcb->events.first;
					ProcessEvent* proc1 = (ProcessEvent*)os->running_one->events.first;
					if(proc->duration < proc1->duration){
						printf("found a process with lower cpu burst than the actual one in cpu 1,dispatching it \n \n");
						FakePCB* pcb1 = (FakePCB*)List_detach(&os->queue[0],(ListItem*)pcb);
						List_pushBack(&os->queue[0],(ListItem*)os->running_one);
						pcb1->last_task_IO = 0;
						os->running_one = pcb1;
						printf("done \n \n");
					}
				break;
				case 2 :
					if(just_dispatched_two){
						break;
					}
					FakePCB* pcb1 = minCpuBurst(os); //pcb that has to be updated with the lowest cpu burst value process
					ProcessEvent* events = (ProcessEvent*) pcb1->events.first;
					ProcessEvent* events1 = (ProcessEvent*)os->running_two->events.first;
					if(events->duration < events1->duration){
						printf("found a process with lower cpu burst than the actual one in cpu 2,dispatching it \n \n");
						FakePCB* pcbl = (FakePCB*)List_detach(&os->queue[0],(ListItem*)pcb1);
						List_pushBack(&os->queue[0],(ListItem*)os->running_two);
						pcbl->last_task_IO = 0;
						os->running_two = pcbl;
						printf("done \n \n");
					}
				break;
				case 3 :
					if(just_dispatched_three){
						break;
					}
					FakePCB* pcb2 = minCpuBurst(os); //pcb that has to be updated with the lowest cpu burst value process
					ProcessEvent* event = (ProcessEvent*) pcb2->events.first;
					ProcessEvent* event1 = (ProcessEvent*)os->running_three->events.first;
					if(event->duration < event1->duration){
						printf("found a process with lower cpu burst than the actual one in cpu 3,dispatching it \n \n");
						FakePCB* pcbm = (FakePCB*)List_detach(&os->queue[0],(ListItem*)pcb2);
						List_pushBack(&os->queue[0],(ListItem*)os->running_three);
						pcbm->last_task_IO = 0;
						os->running_three = pcbm;
						printf("done \n \n");
					}
				break;
				case 4 :
					if(just_dispatched_four){
						break;
					}
					FakePCB* pcb3 = minCpuBurst(os); //pcb that has to be updated with the lowest cpu burst value process
					ProcessEvent* proc3 = (ProcessEvent*) pcb3->events.first;
					ProcessEvent* event3 = (ProcessEvent*)os->running_four->events.first;
					if(proc3->duration < event3->duration){
						printf("found a process with lower cpu burst than the actual one in cpu 4,dispatching it \n \n");
						FakePCB* pcb3m = (FakePCB*)List_detach(&os->queue[0],(ListItem*)pcb3);
						List_pushBack(&os->queue[0],(ListItem*)os->running_four);
						pcb3m->last_task_IO = 0;
						os->running_four = pcb3m;
						printf("done \n \n");
					}
				break;
			}
		}
	}
	printf("DONE \n \n");	
}


/* +++++++++++++++++++++++++++++++++++++ SCHEDULER ROUND ROBIN++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/



void schedRR(FakeOS* os, void* args_){
	printf("SCHEDULER ROUND ROBIN \n \n");
	SchedRRArgs* args=(SchedRRArgs*)args_;

	// look for the first process in ready
	// if none, return
	if (! os->queue[0].first) {
		printf("DONE NO PROCESSES READY \n \n");
		return;
	}
	//verify which cpu are idle dispatch new processes in these cpus 
	if(!os->running_one){
		FakePCB* pcb=(FakePCB*) List_popFront(&os->queue[0]);
		pcb->last_task_IO = 0;
		os->running_one=pcb;
		assert(pcb->events.first);
		ProcessEvent* e = (ProcessEvent*)pcb->events.first;
		assert(e->type==CPU);
		printf("process %d has been dispatched into CPU 1 \n \n",pcb->pid);
		// look at the first event	
		// if duration>quantum
		// push front in the list of event a CPU event of duration quantum
		// alter the duration of the old event subtracting quantum
		if (e->duration>args->quantum) {
			printf("process %d  will take more time to complete a task \n \n",pcb->pid);
			ProcessEvent* qe=(ProcessEvent*)malloc(sizeof(ProcessEvent));
			qe->list.prev=qe->list.next=0;
			qe->type=CPU;
			qe->duration=args->quantum;
			e->duration -= args->quantum;
			List_pushFront(&pcb->events, (ListItem*)qe);
		}
	}
	if(!os->running_two){
		if(!os->queue[0].first){
			return ;
		}
		FakePCB* pcb=(FakePCB*) List_popFront(&os->queue[0]);
		pcb->last_task_IO = 0;
		os->running_two=pcb;
		assert(pcb->events.first);
		ProcessEvent* e = (ProcessEvent*)pcb->events.first;
		assert(e->type==CPU);
		printf("process %d has been dispatched into CPU 2 \n \n",pcb->pid);
		// look at the first event	
		// if duration>quantum
		// push front in the list of event a CPU event of duration quantum
		// alter the duration of the old event subtracting quantum
		if (e->duration>args->quantum) {
			printf("process %d  will take more time to complete a task \n \n",pcb->pid);
			ProcessEvent* qe=(ProcessEvent*)malloc(sizeof(ProcessEvent));
			qe->list.prev=qe->list.next=0;
			qe->type=CPU;
			qe->duration=args->quantum;
			e->duration -= args->quantum;
			List_pushFront(&pcb->events, (ListItem*)qe);
		}
	}
	if(!os->running_three){
		if(!os->queue[0].first){
			return ;
		}
		FakePCB* pcb=(FakePCB*) List_popFront(&os->queue[0]);
		pcb->last_task_IO = 0;
		os->running_three=pcb;
		assert(pcb->events.first);
		ProcessEvent* e = (ProcessEvent*)pcb->events.first;
		assert(e->type==CPU);
		printf("process %d has been dispatched into CPU 3 \n \n",pcb->pid);
		// look at the first event	
		// if duration>quantum
		// push front in the list of event a CPU event of duration quantum
		// alter the duration of the old event subtracting quantum
		if (e->duration>args->quantum) {
			printf("process %d  will take more time to complete a task \n \n",pcb->pid);
			ProcessEvent* qe=(ProcessEvent*)malloc(sizeof(ProcessEvent));
			qe->list.prev=qe->list.next=0;
			qe->type=CPU;
			qe->duration=args->quantum;
			e->duration -= args->quantum;
			List_pushFront(&pcb->events, (ListItem*)qe);
		}
	}
	if(!os->running_four){
		if(!os->queue[0].first){
			return ;
		}
		FakePCB* pcb=(FakePCB*) List_popFront(&os->queue[0]);
		pcb->last_task_IO = 0;
		os->running_four=pcb;
		assert(pcb->events.first);
		ProcessEvent* e = (ProcessEvent*)pcb->events.first;
		assert(e->type==CPU);
		printf("process %d has been dispatched into CPU 4 \n \n",pcb->pid);
		// look at the first event	
		// if duration>quantum
		// push front in the list of event a CPU event of duration quantum
		// alter the duration of the old event subtracting quantum
		if (e->duration>args->quantum) {
			printf("process %d  will take more time to complete a task \n \n",pcb->pid);
			ProcessEvent* qe=(ProcessEvent*)malloc(sizeof(ProcessEvent));
			qe->list.prev=qe->list.next=0;
			qe->type=CPU;
			qe->duration=args->quantum;
			e->duration -= args->quantum;
			List_pushFront(&pcb->events, (ListItem*)qe);
		}
	}
	printf("DONE \n \n");
}


/* +++++++++++++++++++++++++++++++++++++ SCHEDULER Multilevel Feedback Queue++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/


void schedMFQ(FakeOS* os , void*args){ 
	printf("SCHEDULER MULTI LEVEL FEEDBACK \n \n");
	SchedMFQArgs* mfq_args = (SchedMFQArgs*)args;
	//aging policy,if processes in queues have a value for aging_aux equals to 0 then they need to be moved to the higher queue
	if(os->queue[1].first){
		ListItem* item = os->queue[1].first;
		while(item){
			FakePCB* pcb_aux = (FakePCB*)item;
			pcb_aux->aging_aux--;
			if(!pcb_aux->aging_aux){
				FakePCB* aux = (FakePCB*)List_detach(&os->queue[1],(ListItem*)pcb_aux);
				aux->aging_aux = 5;
				aux->queue = 0;
				List_pushBack(&os->queue[0],(ListItem*)aux);
				printf("process %d reached max waiting time in queue 1,moving to queue 0 \n \n",aux->pid);
			}
			item = item->next;
		}
	}
	
	if(os->queue[2].first){
		ListItem* item1 = os->queue[2].first;
		while(item1){
			FakePCB* pcb_aux1 = (FakePCB*)item1;
			pcb_aux1->aging_aux--;
			if(!pcb_aux1->aging_aux){
				FakePCB* aux1 = (FakePCB*)List_detach(&os->queue[2],(ListItem*)pcb_aux1);
				aux1->aging_aux = 5;
				aux1->queue = 1;
				printf("process %d reached max waiting time in queue 2,moving to queue 1 \n \n",aux1->pid);
				List_pushBack(&os->queue[1],(ListItem*)aux1);
			}
			item1 = item1->next;
		}
	}
				
	int running_one_dispatched = 0; //control variable;
	//check which process to put running among the queues,if exist
	if(!os->running_one){ 
		printf("no processes running on 1st CPU \n \n");
		//move inside cpu the process with highest prioirity,i.e. the first process in the highest queue
		if(!os->queue[0].first){
			printf("no processes on queue 0 \n \n");
			if(!os->queue[1].first){
				printf("no processes on queue 1 \n \n");
				if(!os->queue[2].first){
					printf("no processes on queue 2,all busy or not created yet \n \n");
					//processes busy in device queues or not created
					return;
				} else {
					printf("found process on queue 2 \n \n");
					FakePCB* pcb = (FakePCB*)List_popFront(&os->queue[2]);
					pcb->last_task_IO = 0;
					os->running_one= pcb;
					//check cpu burst as schedRR
					assert(pcb->events.first);
					ProcessEvent* e = (ProcessEvent*)pcb->events.first;
					assert(e->type==CPU); 
					printf("process %d has been dispatched into CPU 1 \n \n",pcb->pid);
					running_one_dispatched = 1;
					if (e->duration>mfq_args->quantum[2]) {
						printf("process %d will take more time to complete a task \n \n",pcb->pid);
						ProcessEvent* qe=(ProcessEvent*)malloc(sizeof(ProcessEvent));
						qe->list.prev=qe->list.next=0;
						qe->type=CPU;
						qe->duration=mfq_args->quantum[2];
						e->duration -= mfq_args->quantum[2];
						List_pushFront(&pcb->events, (ListItem*)qe);
					} 
					printf("DONE \n \n");
					
				}
			} else {
				
				printf("found process on queue 1 \n \n");
				FakePCB* pcb = (FakePCB*)List_popFront(&os->queue[1]);
				pcb->last_task_IO = 0;
				os->running_one = pcb;
				//check cpu burst as schedRR
				assert(pcb->events.first);
				ProcessEvent* e = (ProcessEvent*)pcb->events.first;
				assert(e->type==CPU); 
				printf("process %d has been dispatched into CPU 1 \n \n",pcb->pid);
				running_one_dispatched = 1;
				if (e->duration>mfq_args->quantum[1]) {
					printf("cpu burst will take more time than the quantum in queue 1, going to be downgraded after the first quantum expires \n \n");
					pcb->to_downgrade = 1;
					ProcessEvent* qe=(ProcessEvent*)malloc(sizeof(ProcessEvent));
					qe->list.prev=qe->list.next=0;
					qe->type=CPU;
					qe->duration=mfq_args->quantum[1];
					e->duration -= mfq_args->quantum[1];
					List_pushFront(&pcb->events, (ListItem*)qe);
				} 
				printf("DONE \n \n");
			}			
		} else {
			
			printf("found process on queue 0 \n \n");
			FakePCB* pcb = (FakePCB*)List_popFront(&os->queue[0]);
			pcb->last_task_IO = 0;
			os->running_one = pcb;
			//check cpu burst as schedRR
			assert(pcb->events.first);
			ProcessEvent* e = (ProcessEvent*)pcb->events.first;
			assert(e->type==CPU);
			printf("process %d has been dispatched into CPU 1 \n \n",pcb->pid);
			running_one_dispatched = 1;
			if (e->duration>mfq_args->quantum[0]) {
				printf("cpu burst will take more time than the quantum in queue 0, going to be downgraded after the first quantum expires \n \n");
				pcb->to_downgrade = 1;
				ProcessEvent* qe=(ProcessEvent*)malloc(sizeof(ProcessEvent));
				qe->list.prev=qe->list.next=0;
				qe->type=CPU;
				qe->duration=mfq_args->quantum[0];
				e->duration -= mfq_args->quantum[0];
				List_pushFront(&pcb->events, (ListItem*)qe);
			} 
			printf("DONE \n \n");			
		}
	}
	//end of check
	int running_two_dispatched = 0;
	if(!os->running_two){ 
		printf("no processes running on 2nd CPU \n \n");
		//move inside cpu the process with highest prioirity,i.e. the first process in the highest queue
		if(!os->queue[0].first){
			printf("no processes on queue 0 \n \n");
			if(!os->queue[1].first){
				printf("no processes on queue 1 \n \n");
				if(!os->queue[2].first){
					printf("no processes on queue 2,all busy or not created yet \n \n");
					//processes busy in device queues or not created
					return;
				} else {
					printf("found process on queue 2 \n \n");
					FakePCB* pcb = (FakePCB*)List_popFront(&os->queue[2]);
					pcb->last_task_IO = 0;
					os->running_two= pcb;
					//check cpu burst as schedRR
					assert(pcb->events.first);
					ProcessEvent* e = (ProcessEvent*)pcb->events.first;
					assert(e->type==CPU);
					printf("process %d has been dispatched into CPU 2 \n \n",pcb->pid);
					running_two_dispatched = 1;
					if (e->duration>mfq_args->quantum[2]) {
						printf("process %d will take more time to complete a task \n \n",pcb->pid);
						ProcessEvent* qe=(ProcessEvent*)malloc(sizeof(ProcessEvent));
						qe->list.prev=qe->list.next=0;
						qe->type=CPU;
						qe->duration=mfq_args->quantum[2];
						e->duration -= mfq_args->quantum[2];
						List_pushFront(&pcb->events, (ListItem*)qe);
					} 
					printf("DONE \n \n");
				}
			} else {
				printf("found process on queue 1 \n \n");
				FakePCB* pcb = (FakePCB*)List_popFront(&os->queue[1]);
				pcb->last_task_IO = 0;
				os->running_two = pcb;
				//check cpu burst as schedRR
				assert(pcb->events.first);
				ProcessEvent* e = (ProcessEvent*)pcb->events.first;
				assert(e->type==CPU); 
				printf("process %d has been dispatched into CPU 2 \n \n",pcb->pid);
				running_two_dispatched = 1;
				if (e->duration>mfq_args->quantum[1]) {
					printf("cpu burst will take more time than the quantum in queue 1, going to be downgraded after the first quantum expires \n \n");
					pcb->to_downgrade = 1;
					ProcessEvent* qe=(ProcessEvent*)malloc(sizeof(ProcessEvent));
					qe->list.prev=qe->list.next=0;
					qe->type=CPU;
					qe->duration=mfq_args->quantum[1];
					e->duration -= mfq_args->quantum[1];
					List_pushFront(&pcb->events, (ListItem*)qe);
				} 
				printf("DONE \n \n");
			}			
		} else {
			printf("found process on queue 0 \n \n");
			FakePCB* pcb = (FakePCB*)List_popFront(&os->queue[0]);
			pcb->last_task_IO = 0;
			os->running_two = pcb;
			//check cpu burst as schedRR
			assert(pcb->events.first);
			ProcessEvent* e = (ProcessEvent*)pcb->events.first;
			assert(e->type==CPU);
			printf("process %d has been dispatched into CPU 2 \n \n",pcb->pid);
			running_two_dispatched = 1;
			if (e->duration>mfq_args->quantum[0]) {
				printf("cpu burst will take more time than the quantum in queue 0, going to be downgraded after the first quantum expires \n \n");
				pcb->to_downgrade = 1;
				ProcessEvent* qe=(ProcessEvent*)malloc(sizeof(ProcessEvent));
				qe->list.prev=qe->list.next=0;
				qe->type=CPU;
				qe->duration=mfq_args->quantum[0];
				e->duration -= mfq_args->quantum[0];
				List_pushFront(&pcb->events, (ListItem*)qe);
			} 
			printf("DONE \n \n");
		}
	}
	//******************************
	int running_three_dispatched = 0;
	if(!os->running_three){ 
		printf("no processes running on 3rd CPU \n \n");
		//move inside cpu the process with highest prioirity,i.e. the first process in the highest queue
		if(!os->queue[0].first){
			printf("no processes on queue 0 \n \n");
			if(!os->queue[1].first){
				printf("no processes on queue 1 \n \n");
				if(!os->queue[2].first){
					printf("no processes on queue 2,all busy or not created yet \n \n");
					//processes busy in device queues or not created
					return;
				} else {
					printf("found process on queue 2 \n \n");
					FakePCB* pcb = (FakePCB*)List_popFront(&os->queue[2]);
					pcb->last_task_IO = 0;
					os->running_three= pcb;
					//check cpu burst as schedRR
					assert(pcb->events.first);
					ProcessEvent* e = (ProcessEvent*)pcb->events.first;
					assert(e->type==CPU);
					printf("process %d has been dispatched into CPU 3 \n \n",pcb->pid);
					running_three_dispatched = 1;
					if (e->duration>mfq_args->quantum[2]) {
						printf("process %d will take more time to complete a task \n \n",pcb->pid);
						ProcessEvent* qe=(ProcessEvent*)malloc(sizeof(ProcessEvent));
						qe->list.prev=qe->list.next=0;
						qe->type=CPU;
						qe->duration=mfq_args->quantum[2];
						e->duration -= mfq_args->quantum[2];
						List_pushFront(&pcb->events, (ListItem*)qe);
					} 
					printf("DONE \n \n");
				}
			} else {
				printf("found process on queue 1 \n \n");
				FakePCB* pcb = (FakePCB*)List_popFront(&os->queue[1]);
				pcb->last_task_IO = 0;
				os->running_three = pcb;
				//check cpu burst as schedRR
				assert(pcb->events.first);
				ProcessEvent* e = (ProcessEvent*)pcb->events.first;
				assert(e->type==CPU); 
				printf("process %d has been dispatched into CPU 3 \n \n",pcb->pid);
				running_three_dispatched = 1;
				if (e->duration>mfq_args->quantum[1]) {
					printf("cpu burst will take more time than the quantum in queue 1, going to be downgraded after the first quantum expires \n \n");
					pcb->to_downgrade = 1;
					ProcessEvent* qe=(ProcessEvent*)malloc(sizeof(ProcessEvent));
					qe->list.prev=qe->list.next=0;
					qe->type=CPU;
					qe->duration=mfq_args->quantum[1];
					e->duration -= mfq_args->quantum[1];
					List_pushFront(&pcb->events, (ListItem*)qe);
				} 
				printf("DONE \n \n");
			}			
		} else {
			printf("found process on queue 0 \n \n");
			FakePCB* pcb = (FakePCB*)List_popFront(&os->queue[0]);
			pcb->last_task_IO = 0;
			os->running_three = pcb;
			//check cpu burst as schedRR
			assert(pcb->events.first);
			ProcessEvent* e = (ProcessEvent*)pcb->events.first;
			assert(e->type==CPU); 
			printf("process %d has been dispatched into CPU 3 \n \n",pcb->pid);
			running_three_dispatched = 1;
			if (e->duration>mfq_args->quantum[0]) {
				printf("cpu burst will take more time than the quantum in queue 0, going to be downgraded after the first quantum expires \n \n");
				pcb->to_downgrade = 1;
				ProcessEvent* qe=(ProcessEvent*)malloc(sizeof(ProcessEvent));
				qe->list.prev=qe->list.next=0;
				qe->type=CPU;
				qe->duration=mfq_args->quantum[0];
				e->duration -= mfq_args->quantum[0];
				List_pushFront(&pcb->events, (ListItem*)qe);
			} 
			printf("DONE \n \n");
		}
	}
	//******************************
	int running_four_dispatched = 0;
	if(!os->running_four){ 
		printf("no processes running on 4th CPU \n \n");
		//move inside cpu the process with highest prioirity,i.e. the first process in the highest queue
		if(!os->queue[0].first){
			printf("no processes on queue 0 \n \n");
			if(!os->queue[1].first){
				printf("no processes on queue 1 \n \n");
				if(!os->queue[2].first){
					printf("no processes on queue 2,all busy or not created yet \n \n");
					//processes busy in device queues or not created
					return;
				} else {
					printf("found process on queue 2 \n \n");
					FakePCB* pcb = (FakePCB*)List_popFront(&os->queue[2]);
					pcb->last_task_IO = 0;
					os->running_four= pcb;
					//check cpu burst as schedRR
					assert(pcb->events.first);
					ProcessEvent* e = (ProcessEvent*)pcb->events.first;
					assert(e->type==CPU); 
					printf("process %d has been dispatched into CPU 4 \n \n",pcb->pid);
					running_four_dispatched = 1;
					if (e->duration>mfq_args->quantum[2]) {
						printf("process %d will take more time to complete a task \n \n",pcb->pid);
						ProcessEvent* qe=(ProcessEvent*)malloc(sizeof(ProcessEvent));
						qe->list.prev=qe->list.next=0;
						qe->type=CPU;
						qe->duration=mfq_args->quantum[2];
						e->duration -= mfq_args->quantum[2];
						List_pushFront(&pcb->events, (ListItem*)qe);
					} 
					printf("DONE \n \n");
				}
			} else {
				printf("found process on queue 1 \n \n");
				FakePCB* pcb = (FakePCB*)List_popFront(&os->queue[1]);
				pcb->last_task_IO = 0;
				os->running_four = pcb;
				//check cpu burst as schedRR
				assert(pcb->events.first);
				ProcessEvent* e = (ProcessEvent*)pcb->events.first;
				assert(e->type==CPU); 
				printf("process %d has been dispatched into CPU 4 \n \n",pcb->pid);
				running_four_dispatched = 1;
				if (e->duration>mfq_args->quantum[1]) {
					printf("cpu burst will take more time than the quantum in queue 1, going to be downgraded after the first quantum expires \n \n");
					pcb->to_downgrade = 1;
					ProcessEvent* qe=(ProcessEvent*)malloc(sizeof(ProcessEvent));
					qe->list.prev=qe->list.next=0;
					qe->type=CPU;
					qe->duration=mfq_args->quantum[1];
					e->duration -= mfq_args->quantum[1];
					List_pushFront(&pcb->events, (ListItem*)qe);
				} 
				printf("DONE \n \n");
			}			
		} else {
			printf("found process on queue 0 \n \n");
			FakePCB* pcb = (FakePCB*)List_popFront(&os->queue[0]);
			pcb->last_task_IO = 0;
			os->running_four = pcb;
			//check cpu burst as schedRR
			assert(pcb->events.first);
			ProcessEvent* e = (ProcessEvent*)pcb->events.first;
			assert(e->type==CPU); 
			printf("process %d has been dispatched into CPU 4 \n \n",pcb->pid);
			running_four_dispatched = 1;
			if (e->duration>mfq_args->quantum[0]) {
				printf("cpu burst will take more time than the quantum in queue 0, going to be downgraded after the first quantum expires \n \n");
				pcb->to_downgrade = 1;
				ProcessEvent* qe=(ProcessEvent*)malloc(sizeof(ProcessEvent));
				qe->list.prev=qe->list.next=0;
				qe->type=CPU;
				qe->duration=mfq_args->quantum[0];
				e->duration -= mfq_args->quantum[0];
				List_pushFront(&pcb->events, (ListItem*)qe);
			} 
			printf("DONE \n \n");
		}
	}
	//**********************
	
	//if the process was bound to queue 1 and a process in queue 0 exists, we must put as running the latter and pushback the actual running process in queue 1
	if(!running_one_dispatched){
		if(os->running_one->queue == 1){
			printf("checking if a process with higher priority is available \n \n");
			if(os->queue[0].first){
				printf("found a process in queue 0 \n \n ");
				//pushing back current running process to queue 1
				os->running_one->aging_aux = 5; //reset aging value
				os->running_one->to_downgrade = 0;
				List_pushBack(&os->queue[1],(ListItem*)os->running_one);
				FakePCB* pcbToMove = (FakePCB*)List_popFront(&os->queue[0]);
				printf("found process %d in queue 0, going to run now! \n \n",pcbToMove->pid);
				pcbToMove->last_task_IO = 0;
				os->running_one = pcbToMove;
				assert(pcbToMove->events.first);
				ProcessEvent* pe = (ProcessEvent*)pcbToMove->events.first;
				assert(pe->type==CPU); 
				if (pe->duration>mfq_args->quantum[0]) {
					printf("cpu burst will take more time than the quantum in queue 0, going to be downgraded after the first quantum expires \n \n");
					pcbToMove->to_downgrade = 1;
					ProcessEvent* qel=(ProcessEvent*)malloc(sizeof(ProcessEvent));
					qel->list.prev=qel->list.next=0;
					qel->type=CPU;
					qel->duration=mfq_args->quantum[0];
					pe->duration -= mfq_args->quantum[0];
					List_pushFront(&pcbToMove->events, (ListItem*)qel);
				} 
				printf("DONE \n \n");
			
			} else {
				printf("DONE \n \n");
			}
		}else if(os->running_one->queue ==2){	//if the process was bound to queue 2 and a process in queue 0 or 1 exists, we must put as running the one within the highest queue and pushback the actual running process in queue 2
			printf("checking if a process with higher priority is available \n \n");
			if(os->queue[0].first){
				//pushing back current running process to queue 2
				os->running_one->aging_aux = 5; //reset aging value
				os->running_one->to_downgrade = 0;
				List_pushBack(&os->queue[2],(ListItem*)os->running_one);
				FakePCB* pcbToMove = (FakePCB*)List_popFront(&os->queue[0]);
				pcbToMove->last_task_IO = 0;
				printf("found process %d in queue 0, going to run now! \n \n",pcbToMove->pid); 
				os->running_one = pcbToMove;
				assert(pcbToMove->events.first);
				ProcessEvent* pe = (ProcessEvent*)pcbToMove->events.first;
				assert(pe->type==CPU); 
				if (pe->duration>mfq_args->quantum[0]) {
					printf("cpu burst will take more time than the quantum in queue 0, going to be downgraded after the first quantum expires \n \n");
					pcbToMove->to_downgrade = 1;
					ProcessEvent* qel=(ProcessEvent*)malloc(sizeof(ProcessEvent));
					qel->list.prev=qel->list.next=0;
					qel->type=CPU;
					qel->duration=mfq_args->quantum[0];
					pe->duration -= mfq_args->quantum[0];
					List_pushFront(&pcbToMove->events, (ListItem*)qel);
				}
				printf("DONE \n \n");
			} else if(os->queue[1].first){
				os->running_one->aging_aux = 5; //reset aging value
				os->running_one->to_downgrade = 0;
				List_pushBack(&os->queue[2],(ListItem*)os->running_one);
				FakePCB* pcbToMove = (FakePCB*)List_popFront(&os->queue[1]);
				pcbToMove->last_task_IO = 0;
				printf("found process %d in queue 1, going to run now! \n \n",pcbToMove->pid); 
				os->running_one = pcbToMove;
				assert(pcbToMove->events.first);
				ProcessEvent* pe = (ProcessEvent*)pcbToMove->events.first;
				assert(pe->type==CPU); 
				if (pe->duration>mfq_args->quantum[1]) {
					printf("cpu burst will take more time than the quantum in queue 1, going to be downgraded after the first quantum expires \n \n");
					pcbToMove->to_downgrade = 1;
					ProcessEvent* qel=(ProcessEvent*)malloc(sizeof(ProcessEvent));
					qel->list.prev=qel->list.next=0;
					qel->type=CPU;
					qel->duration=mfq_args->quantum[1];
					pe->duration -= mfq_args->quantum[1];
					List_pushFront(&pcbToMove->events, (ListItem*)qel);
				}
				printf("DONE \n \n");
			} else {
				//no processes in either queue 0 or 1		
				printf("DONE \n \n");
			}
		}
	}
	
	//end of check
	
	//if the process was bound to queue 1 and a process in queue 0 exists, we must put as running the latter and pushback the actual running process in queue 1
	//cpu 2 part 2
	if(!running_two_dispatched){
		if(os->running_two->queue == 1){
			printf("checking if a process with higher priority is available \n \n");
			if(os->queue[0].first){
				printf("found a process in queue 0 \n \n ");
				//pushing back current running process to queue 1
				os->running_two->aging_aux = 5; //reset aging value
				os->running_two->to_downgrade = 0;
				List_pushBack(&os->queue[1],(ListItem*)os->running_two);
				FakePCB* pcbToMove = (FakePCB*)List_popFront(&os->queue[0]);
				printf("found process %d in queue 0, going to run now! \n \n",pcbToMove->pid);
				pcbToMove->last_task_IO = 0;
				os->running_two = pcbToMove;
				assert(pcbToMove->events.first);
				ProcessEvent* pe = (ProcessEvent*)pcbToMove->events.first;
				assert(pe->type==CPU); 
				if (pe->duration>mfq_args->quantum[0]) {
					printf("cpu burst will take more time than the quantum in queue 0, going to be downgraded after the first quantum expires \n \n");
					pcbToMove->to_downgrade = 1;
					ProcessEvent* qel=(ProcessEvent*)malloc(sizeof(ProcessEvent));
					qel->list.prev=qel->list.next=0;
					qel->type=CPU;
					qel->duration=mfq_args->quantum[0];
					pe->duration -= mfq_args->quantum[0];
					List_pushFront(&pcbToMove->events, (ListItem*)qel);
				} 
				printf("DONE \n \n");
			
			} else {
				printf("DONE \n \n");
			}
		} else if(os->running_two->queue ==2){	//if the process was bound to queue 2 and a process in queue 0 or 1 exists, we must put as running the one within the highest queue and pushback the actual running process in queue 2
			printf("checking if a process with higher priority is available \n \n");
			if(os->queue[0].first){
				//pushing back current running process to queue 2
				os->running_two->aging_aux = 5; //reset aging value
				os->running_two->to_downgrade = 0;
				List_pushBack(&os->queue[2],(ListItem*)os->running_two);
				FakePCB* pcbToMove = (FakePCB*)List_popFront(&os->queue[0]);
				printf("found process %d in queue 0, going to run now! \n \n",pcbToMove->pid);
				pcbToMove->last_task_IO = 0;
				os->running_two = pcbToMove;
				assert(pcbToMove->events.first);
				ProcessEvent* pe = (ProcessEvent*)pcbToMove->events.first;
				assert(pe->type==CPU); 
				if (pe->duration>mfq_args->quantum[0]) {
					printf("cpu burst will take more time than the quantum in queue 0, going to be downgraded after the first quantum expires \n \n");
					pcbToMove->to_downgrade = 1;
					ProcessEvent* qel=(ProcessEvent*)malloc(sizeof(ProcessEvent));
					qel->list.prev=qel->list.next=0;
					qel->type=CPU;
					qel->duration=mfq_args->quantum[0];
					pe->duration -= mfq_args->quantum[0];
					List_pushFront(&pcbToMove->events, (ListItem*)qel);
				}
				printf("DONE \n \n");
			} else if(os->queue[1].first){
				os->running_two->aging_aux = 5; //reset aging value
				os->running_two->to_downgrade = 0;//reset downgrade flag(prossima iterazione sicuramente cpu burst < quantum)
				List_pushBack(&os->queue[2],(ListItem*)os->running_two);
				FakePCB* pcbToMove = (FakePCB*)List_popFront(&os->queue[1]);
				printf("found process %d in queue 1, going to run now! \n \n",pcbToMove->pid);
				pcbToMove->last_task_IO = 0;
				os->running_two = pcbToMove;
				assert(pcbToMove->events.first);
				ProcessEvent* pe = (ProcessEvent*)pcbToMove->events.first;
				assert(pe->type==CPU); 
				if (pe->duration>mfq_args->quantum[1]) {
					printf("cpu burst will take more time than the quantum in queue 1, going to be downgraded after the first quantum expires \n \n");
					pcbToMove->to_downgrade = 1;
					ProcessEvent* qel=(ProcessEvent*)malloc(sizeof(ProcessEvent));
					qel->list.prev=qel->list.next=0;
					qel->type=CPU;
					qel->duration=mfq_args->quantum[1];
					pe->duration -= mfq_args->quantum[1];
					List_pushFront(&pcbToMove->events, (ListItem*)qel);
				}
				printf("DONE \n \n");
			} else {
			//no processes in either queue 0 or 1		
			printf("DONE \n \n");
			}
		}
	}
	//*******************************************************************
	
	//end of check
	//if the process was bound to queue 1 and a process in queue 0 exists, we must put as running the latter and pushback the actual running process in queue 1
	//cpu 3 part 2
	if(!running_three_dispatched){
		if(os->running_three->queue == 1){
			printf("checking if a process with higher priority is available \n \n");
			if(os->queue[0].first){
				printf("found a process in queue 0 \n \n ");
				//pushing back current running process to queue 1
				os->running_three->aging_aux = 5; //reset aging value
				os->running_three->to_downgrade = 0;
				List_pushBack(&os->queue[1],(ListItem*)os->running_three);
				FakePCB* pcbToMove = (FakePCB*)List_popFront(&os->queue[0]);
				printf("found process %d in queue 0, going to run now! \n \n",pcbToMove->pid); 
				pcbToMove->last_task_IO = 0;
				os->running_three = pcbToMove;
				assert(pcbToMove->events.first);
				ProcessEvent* pe = (ProcessEvent*)pcbToMove->events.first;
				assert(pe->type==CPU); 
				if (pe->duration>mfq_args->quantum[0]) {
					printf("cpu burst will take more time than the quantum in queue 0, going to be downgraded after the first quantum expires \n \n");
					pcbToMove->to_downgrade = 1;
					ProcessEvent* qel=(ProcessEvent*)malloc(sizeof(ProcessEvent));
					qel->list.prev=qel->list.next=0;
					qel->type=CPU;
					qel->duration=mfq_args->quantum[0];
					pe->duration -= mfq_args->quantum[0];
					List_pushFront(&pcbToMove->events, (ListItem*)qel);
				} 
				printf("DONE \n \n");
			
			} else {
				printf("DONE \n \n");
			}
		} else if(os->running_three->queue ==2){	//if the process was bound to queue 2 and a process in queue 0 or 1 exists, we must put as running the one within the highest queue and pushback the actual running process in queue 2
			printf("checking if a process with higher priority is available \n \n");
			if(os->queue[0].first){
				//pushing back current running process to queue 2
				os->running_three->aging_aux = 5; //reset aging value
				os->running_three->to_downgrade = 0;
				List_pushBack(&os->queue[2],(ListItem*)os->running_three);
				FakePCB* pcbToMove = (FakePCB*)List_popFront(&os->queue[0]);
				printf("found process %d in queue 0, going to run now! \n \n",pcbToMove->pid);
				pcbToMove->last_task_IO = 0;
				os->running_three = pcbToMove;
				assert(pcbToMove->events.first);
				ProcessEvent* pe = (ProcessEvent*)pcbToMove->events.first;
				assert(pe->type==CPU); 
				if (pe->duration>mfq_args->quantum[0]) {
					printf("cpu burst will take more time than the quantum in queue 0, going to be downgraded after the first quantum expires \n \n");
					pcbToMove->to_downgrade = 1;
					ProcessEvent* qel=(ProcessEvent*)malloc(sizeof(ProcessEvent));
					qel->list.prev=qel->list.next=0;
					qel->type=CPU;
					qel->duration=mfq_args->quantum[0];
					pe->duration -= mfq_args->quantum[0];
					List_pushFront(&pcbToMove->events, (ListItem*)qel);
				}
				printf("DONE \n \n");
			} else if(os->queue[1].first){
				os->running_three->aging_aux = 5; //reset aging value
				os->running_three->to_downgrade = 0;
				List_pushBack(&os->queue[2],(ListItem*)os->running_three);
				FakePCB* pcbToMove = (FakePCB*)List_popFront(&os->queue[1]);
				printf("found process %d in queue 1, going to run now! \n \n",pcbToMove->pid); 
				pcbToMove->last_task_IO = 0;
				os->running_three = pcbToMove;
				assert(pcbToMove->events.first);
				ProcessEvent* pe = (ProcessEvent*)pcbToMove->events.first;
				assert(pe->type==CPU); 
				if (pe->duration>mfq_args->quantum[1]) {
					printf("cpu burst will take more time than the quantum in queue 1, going to be downgraded after the first quantum expires \n \n");
					pcbToMove->to_downgrade = 1;
					ProcessEvent* qel=(ProcessEvent*)malloc(sizeof(ProcessEvent));
					qel->list.prev=qel->list.next=0;
					qel->type=CPU;
					qel->duration=mfq_args->quantum[1];
					pe->duration -= mfq_args->quantum[1];
					List_pushFront(&pcbToMove->events, (ListItem*)qel);
				}
				printf("DONE \n \n");
			} else {
			//no processes in either queue 0 or 1		
				printf("DONE \n \n");
			}
		}
	}
	//***************************************************************
	
	//end of check
	
	//CPU 4 part 2

	//if the process was bound to queue 1 and a process in queue 0 exists, we must put as running the latter and pushback the actual running process in queue 1
	if(!running_four_dispatched){
		if(os->running_four->queue == 1){
			printf("checking if a process with higher priority is available \n \n");
			if(os->queue[0].first){
				printf("found a process in queue 0 \n \n ");
				//pushing back current running process to queue 1
				os->running_four->aging_aux = 5; //reset aging value
				os->running_four->to_downgrade = 0;
				List_pushBack(&os->queue[1],(ListItem*)os->running_four);
				FakePCB* pcbToMove = (FakePCB*)List_popFront(&os->queue[0]);
				printf("found process %d in queue 0, going to run now! \n \n",pcbToMove->pid); 
				pcbToMove->last_task_IO = 0;
				os->running_four = pcbToMove;
				assert(pcbToMove->events.first);
				ProcessEvent* pe = (ProcessEvent*)pcbToMove->events.first;
				assert(pe->type==CPU); 
				if (pe->duration>mfq_args->quantum[0]) {
					printf("cpu burst will take more time than the quantum in queue 0, going to be downgraded after the first quantum expires \n \n");
					pcbToMove->to_downgrade = 1;
					ProcessEvent* qel=(ProcessEvent*)malloc(sizeof(ProcessEvent));
					qel->list.prev=qel->list.next=0;
					qel->type=CPU;
					qel->duration=mfq_args->quantum[0];
					pe->duration -= mfq_args->quantum[0];
					List_pushFront(&pcbToMove->events, (ListItem*)qel);
				} 
				printf("DONE \n \n");
			
			} else {
				printf("DONE \n \n");
			}		
		}else if(os->running_four->queue ==2){	//if the process was bound to queue 2 and a process in queue 0 or 1 exists, we must put as running the one within the highest queue and pushback the actual running process in queue 2
			printf("checking if a process with higher priority is available \n \n");
			if(os->queue[0].first){
				//pushing back current running process to queue 2
				os->running_four->aging_aux = 5; //reset aging value
				os->running_four->to_downgrade = 0;
				List_pushBack(&os->queue[2],(ListItem*)os->running_four);
				FakePCB* pcbToMove = (FakePCB*)List_popFront(&os->queue[0]);
				printf("found process %d in queue 0, going to run now! \n \n",pcbToMove->pid);
				pcbToMove->last_task_IO = 0;
				os->running_four = pcbToMove;
				assert(pcbToMove->events.first);
				ProcessEvent* pe = (ProcessEvent*)pcbToMove->events.first;
				assert(pe->type==CPU); 
				if (pe->duration>mfq_args->quantum[0]) {
					printf("cpu burst will take more time than the quantum in queue 0, going to be downgraded after the first quantum expires \n \n");
					pcbToMove->to_downgrade = 1;
					ProcessEvent* qel=(ProcessEvent*)malloc(sizeof(ProcessEvent));
					qel->list.prev=qel->list.next=0;
					qel->type=CPU;
					qel->duration=mfq_args->quantum[0];
					pe->duration -= mfq_args->quantum[0];
					List_pushFront(&pcbToMove->events, (ListItem*)qel);
				}
				printf("DONE \n \n");
			} else if(os->queue[1].first){
				os->running_four->aging_aux = 5; //reset aging value
				os->running_four->to_downgrade = 0;
				List_pushBack(&os->queue[2],(ListItem*)os->running_four);
				FakePCB* pcbToMove = (FakePCB*)List_popFront(&os->queue[1]);
				printf("found process %d in queue 1, going to run now! \n \n",pcbToMove->pid);
				pcbToMove->last_task_IO = 0;
				os->running_four = pcbToMove;
				assert(pcbToMove->events.first);
				ProcessEvent* pe = (ProcessEvent*)pcbToMove->events.first;
				assert(pe->type==CPU); 
				if (pe->duration>mfq_args->quantum[1]) {
					printf("cpu burst will take more time than the quantum in queue 1, going to be downgraded after the first quantum expires \n \n");
					pcbToMove->to_downgrade = 1;
					ProcessEvent* qel=(ProcessEvent*)malloc(sizeof(ProcessEvent));
					qel->list.prev=qel->list.next=0;
					qel->type=CPU;
					qel->duration=mfq_args->quantum[1];
					pe->duration -= mfq_args->quantum[1];
					List_pushFront(&pcbToMove->events, (ListItem*)qel);
				}
				printf("DONE \n \n");
			} else {
				//no processes in either queue 0 or 1		
				printf("DONE \n \n");
			}
		}
	}
	printf("DONE \n \n");
}

int main(int argc, char** argv) {
  FakeOS_init(&os);
  SchedRRArgs srr_args; //quanto scheduler RR
  SchedMFQArgs mfqargs; //quanto scheduler MFQ
  srr_args.quantum = 7;
  mfqargs.quantum[0] = 7;
  mfqargs.quantum[1] = 10;
  mfqargs.quantum[2] = 15;
  os.schedule_fcfs = schedFCFS;
  os.schedule_sjf = schedSJF;
  os.schedule_rr=schedRR;
  os.schedulerr_args=&srr_args;
  os.schedule_mfq = schedMFQ;
  os.schedulemfq_args = &mfqargs;
  pthread_t server;	
  printf("Start...\n");
  pthread_create(&server, NULL, &queueServer, NULL); //create + start
  pthread_join(server, NULL);
  printf("Done...\n");
  return (EXIT_SUCCESS);
 }

