#pragma once
#include "linked_list.h"

typedef enum {CPU=0, DISK1=1, DISK2 = 2, CAMERA = 3} ResourceType; 



typedef struct {
  ListItem list;
  ResourceType type;
  int duration;
} ProcessEvent;

// fake process
typedef struct {
  ListItem list;
  int pid; // assigned by us
  int arrival_time;
  ListHead events;
  int queue; //used only for mfq scheduler

} FakeProcess;

int FakeProcess_load(FakeProcess* p, const char* filename);

int FakeProcess_save(const FakeProcess* p, const char* filename);
